package com.briswell.photobook

import com.briswell.photobook.api.model.User
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded


interface SystemInfo {

    companion object Constant{
        lateinit var user:User
        var APP_FOLDER_NAME:String = "IJNET_PHOTOBOOK"
    }
    enum class UserRole (val value:Int) {
        SYSTEM_ADMIN(1),ADMIN(2),STAFF(3)
    }
}