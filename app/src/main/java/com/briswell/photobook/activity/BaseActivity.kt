package com.briswell.photobook.activity

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.briswell.photobook.R
import com.google.gson.Gson

/**
 * Created by luc.nt on 4/5/2018.
 */
open class BaseActivity(): AppCompatActivity() {

    lateinit var alertDialogPopup: android.support.v7.app.AlertDialog
    public enum class MESSAGE_TYPE(val value:Long) {
        SUCCESS(1),ERROR(-1),WARNING(0)
    }


    fun dialogPopup(activity: Activity,Type:MESSAGE_TYPE?,msg: String) {
        val inflater = activity.getLayoutInflater()
        val alertLayout = inflater.inflate(R.layout.dialog_notice, null)
        var btnOk = alertLayout.findViewById<Button>(R.id.btnOk)
        var title = alertLayout.findViewById<TextView>(R.id.title)
        var message = alertLayout.findViewById<TextView>(R.id.message)
        var header = alertLayout.findViewById<LinearLayout>(R.id.header)
        if(Type == MESSAGE_TYPE.ERROR) {
            header.setBackgroundColor(activity.resources.getColor(R.color.colorRed))
            btnOk.setBackgroundColor(activity.resources.getColor(R.color.colorRed))
            title.setText("エラー")
        }else if(Type == MESSAGE_TYPE.WARNING) {
            header.setBackgroundColor( activity.resources.getColor(R.color.colorYellow))
            btnOk.setBackgroundColor(activity.resources.getColor(R.color.colorYellow))
            title.setText("警告")
        }
        message.setText(msg)
        val alert = android.support.v7.app.AlertDialog.Builder(activity)
        alert.setView(alertLayout)
        alert.setCancelable(false)
        alertDialogPopup = alert.create()
        btnOk.setOnClickListener(){
            closeDialog()
        }
        val window = alertDialogPopup.getWindow()
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setGravity(Gravity.CENTER)
        alertDialogPopup.show()
    }
    fun dialogPopupAndFinishActivity(activity: Activity,resultIntent:Intent,Type:MESSAGE_TYPE?,msg: String) {
        val inflater = activity.getLayoutInflater()
        val alertLayout = inflater.inflate(R.layout.dialog_notice, null)
        var btnOk = alertLayout.findViewById<Button>(R.id.btnOk)
        var title = alertLayout.findViewById<TextView>(R.id.title)
        var message = alertLayout.findViewById<TextView>(R.id.message)
        var header = alertLayout.findViewById<LinearLayout>(R.id.header)
        if(Type == MESSAGE_TYPE.ERROR) {
            header.setBackgroundColor(activity.resources.getColor(R.color.colorRed))
            btnOk.setBackgroundColor(activity.resources.getColor(R.color.colorRed))
            title.setText("エラー")
        }else if(Type == MESSAGE_TYPE.WARNING) {
            header.setBackgroundColor( activity.resources.getColor(R.color.colorYellow))
            btnOk.setBackgroundColor(activity.resources.getColor(R.color.colorYellow))
            title.setText("警告")
        }
        message.setText(msg)
        val alert = android.support.v7.app.AlertDialog.Builder(activity)
        alert.setView(alertLayout)
        alert.setCancelable(false)
        alertDialogPopup = alert.create()
        btnOk.setOnClickListener(){
            closeDialog()
            activity.setResult(Activity.RESULT_OK, resultIntent)
            activity.finish()
        }
        val window = alertDialogPopup.getWindow()
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setGravity(Gravity.CENTER)
        alertDialogPopup.show()
    }
    open fun closeDialog() {
        alertDialogPopup.dismiss()
    }
    fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this!!)
                .setMessage(message)
                .setPositiveButton("はい", okListener)
                .setNegativeButton("いいえ", null)
                .create()
                .show()
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev!!.getAction() == MotionEvent.ACTION_DOWN) {

            /**
             * It gets into the above IF-BLOCK if anywhere the screen is touched.
             */

            var v: View = getCurrentFocus();
            if (v is EditText) {


                /**
                 * Now, it gets into the above IF-BLOCK if an EditText is already in focus, and you tap somewhere else
                 * to take the focus away from that particular EditText. It could have 2 cases after tapping:
                 * 1. No EditText has focus
                 * 2. Focus is just shifted to the other EditText
                 */

                var outRect: Rect = Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains(ev!!.getRawX().toInt(), ev!!.getRawY().toInt())) {
                    v.clearFocus();
                    var imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

}