package com.briswell.photobook.activity

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.briswell.photobook.BuildConfig
import com.briswell.photobook.R
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.LoginResult
import com.briswell.photobook.api.model.User
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.model.SendBroadcastReceiverModel
import com.briswell.photobook.util.Constant
import com.briswell.photobook.util.PreferencesHelper
import com.briswell.photobook.util.api.repository.AccountRepositoryProvider
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.File


/**
 * ログイン画面
 * 作成者：luc.nt
 * 作成日：20180403
 */

class S1011Activity : BaseActivity() {
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var email:EditText
    private lateinit var password:EditText
    private lateinit var login:Button
    private lateinit var regist:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            var policy: StrictMode.ThreadPolicy =  StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Constant.APP_CONTEXT = applicationContext
        setContentView(R.layout.activity_s1011)

        Toast.makeText(this, BuildConfig.awsBucket, Toast.LENGTH_LONG).show()
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        try {
            email.setText(PreferencesHelper.getString(applicationContext,"user_name"))
            password.setText(PreferencesHelper.getString(applicationContext,"password"))
        }catch (e:Exception){}

        login = findViewById(R.id.login)
        login.setOnClickListener(){
            login()
        }

        val connMgr = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connMgr.getActiveNetworkInfo()
        if(netInfo != null && netInfo.isConnected()) {
            var json = PreferencesHelper.getString(applicationContext,Constant.UPLOAD_PENDING_PHOTO_DATA)
            if(!json.isNullOrEmpty()) {
                val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
                var items:MutableList<SendBroadcastReceiverModel> = mutableListOf()
                items = Gson().fromJson(json, object : TypeToken<List<SendBroadcastReceiverModel>>() {}.type)
                var newfile: File? = null
                var picturePath:String? = null
                var flg = true
                for (i in 0 until items.count()) {
                    picturePath = items[i].local_file_path
                    newfile = File(picturePath)
                    var call: Call<ResponseBody> = repository.setReportPhotoFile(items[i].cookie!!,items[i].request_id, items[i].path, items[i].folder_flg.toString(), items[i].user_id.toString(), items[i].folder_id, newfile!!)
                    var body: String = call.execute().body()!!.string()
                    Log.d("result:", body)
                    var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
                    if(result.status != IJNetApiService.ApiStatus.OK.value) {
                        flg = false
                    }

                }
                if(flg) {
                    PreferencesHelper.putString(applicationContext,Constant.UPLOAD_PENDING_PHOTO_DATA,"")
                    Toast.makeText(this, resources.getString(R.string.INFO_003), Toast.LENGTH_SHORT).show()

                }else {
                    Toast.makeText(this, resources.getString(R.string.CERR_006), Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
    fun login() {
        if(validate()) {

            val repository = AccountRepositoryProvider.getAccountRepository()
            var call:Call<ResponseBody> = repository.login(IJNetApiService.key, email.text.toString(), password.text.toString())
            var response = call.execute()

            var body:String = response.body()!!.string()
            var result: LoginResult?  = Gson().fromJson(body, LoginResult::class.java)
            if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                IJNetApiService.REQUEST_COOKIES = response.headers().get("Set-Cookie").toString()
                var gson = Gson()
                val user = gson.fromJson(Gson().toJson(result.data as LinkedTreeMap<String, Any>), User::class.java)
                SystemInfo.user = user
                PreferencesHelper.putString(applicationContext,"user_name",email.text.toString())
                PreferencesHelper.putString(applicationContext,"password",password.text.toString())
                val intent = Intent(this@S1011Activity, S1021Activity::class.java)
                startActivity(intent)
            } else {
                dialogPopup(this@S1011Activity,MESSAGE_TYPE.ERROR, result.message)
            }
            Log.d("Result", "${result}")

        }
    }


    override fun closeDialog() {
        super.closeDialog()
        Log.d("dialog","override")

    }
    fun validate():Boolean {
        if(email.text.toString().trim().equals("")){
            dialogPopup(this,MESSAGE_TYPE.ERROR,String.format(this.resources.getString(R.string.CERR_001),"ユーザ名"))
            return false
        }
        if(password.text.toString().trim().equals("")){
            dialogPopup(this,MESSAGE_TYPE.ERROR,String.format(this.resources.getString(R.string.CERR_001),"パスワード"))
            return false
        }
        return true
    }


}
