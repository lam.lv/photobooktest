package com.briswell.photobook.activity

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.briswell.photobook.R
import com.briswell.photobook.adapter.RequestListAdapter
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.helper.OnLoadMoreListener
import com.briswell.photobook.helper.RecyclerViewLoadMoreScroll
import com.briswell.photobook.util.api.model.Request
import com.briswell.photobook.util.api.repository.RequestRepositoryProvider
import com.wang.avi.AVLoadingIndicatorView
import io.reactivex.disposables.CompositeDisposable
import java.text.SimpleDateFormat
import java.util.*
import android.app.Activity
import android.graphics.Rect
import android.os.StrictMode
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.api.GetRequestResult
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat


/**
 * 依頼一覧画面
 * 作成者：luc.nt
 * 作成日：20180404
 */

class S1021Activity : BaseActivity() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var search: Button
    private lateinit var clear:Button
    private lateinit var detailView: RecyclerView
    private lateinit var workingDateFrom:TextView
    private lateinit var workingDateTo:TextView
    private lateinit var locationAreaName:EditText
    private lateinit var locationAreaId:EditText
    private lateinit var ttsOrderId:EditText
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var adminCondition: LinearLayout
    private lateinit var lbSection:TextView
    private lateinit var sectionId:EditText
    private lateinit var sectionName:TextView
    private lateinit var lbStaff:TextView
    private lateinit var staffId:EditText
    private lateinit var staffName:TextView
    private lateinit var lbSupplier:TextView
    private lateinit var supplierId:EditText
    private lateinit var supplierName:TextView
    private lateinit var searchLocationArea:ImageButton
    private lateinit var searchSection:ImageButton
    private lateinit var searchStaff:ImageButton
    private lateinit var searchSupplier:ImageButton
    private lateinit var count:TextView
    private var page = 1
    private var data: MutableList<Request?> = mutableListOf()
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private  lateinit var mAvi: AVLoadingIndicatorView
    private   var viewAdapter:RequestListAdapter? = null
    private var loading:Boolean = false
    private var context: Context?= null
    private val REQUEST_SEARCH_SECTION  = 100
    private val REQUEST_SEARCH_STAFF  = 101
    private val REQUEST_SEARCH_SUPPLIER  = 102
    private val REQUEST_SEARCH_LOCATION_AREA  = 103
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            var policy: StrictMode.ThreadPolicy =  StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        context = this

        setContentView(R.layout.activity_s1021)

        workingDateFrom = findViewById(R.id.working_date_from)
        workingDateTo = findViewById(R.id.working_date_to)
        locationAreaName = findViewById(R.id.location_area_name)
        locationAreaId = findViewById(R.id.location_area_id)
        ttsOrderId = findViewById(R.id.tts_order_id)
        adminCondition = findViewById(R.id.admin_condition)
        lbSection = findViewById(R.id.lb_section)
        sectionId = findViewById(R.id.section_id)
        sectionName = findViewById(R.id.section_name)
        lbStaff = findViewById(R.id.lb_staff)
        staffId = findViewById(R.id.staff_id)
        staffName = findViewById(R.id.staff_name)
        lbSupplier = findViewById(R.id.lb_supplier)
        supplierId = findViewById(R.id.supplier_id)
        supplierName = findViewById(R.id.supplier_name)
        detailView = findViewById(R.id.construction_list)
        search = findViewById(R.id.search)
        clear = findViewById(R.id.clear)
        count = findViewById(R.id.count)
        mAvi = findViewById(R.id.avi)
        searchLocationArea = findViewById(R.id.search_location_area)
        searchSection = findViewById(R.id.search_section)
        searchStaff = findViewById(R.id.search_staff)
        searchSupplier = findViewById(R.id.search_supplier)

        viewAdapter = RequestListAdapter(this, data)
        detailView!!.adapter = viewAdapter
        var layoutManager: LinearLayoutManager = (detailView.layoutManager as LinearLayoutManager)
        scrollListener = RecyclerViewLoadMoreScroll(layoutManager)
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                Log.d("IJNET", "start load more")
                loadMoreConstructionData()
            }
        })

        detailView.addOnScrollListener(scrollListener)

        searchLocationArea.setOnClickListener() {

            val intent = Intent(this, S1025Activity::class.java)
            startActivityForResult(intent,REQUEST_SEARCH_LOCATION_AREA)
            return@setOnClickListener
        }
        searchSection.setOnClickListener() {

            val intent = Intent(this, S1022Activity::class.java)
            startActivityForResult(intent,REQUEST_SEARCH_SECTION)
            return@setOnClickListener
        }
        searchStaff.setOnClickListener() {
            val intent = Intent(this, S1023Activity::class.java)
            intent.putExtra("sectionId", sectionId.text)
            startActivityForResult(intent,REQUEST_SEARCH_STAFF)
            return@setOnClickListener
        }
        searchSupplier.setOnClickListener() {
            val intent = Intent(this, S1024Activity::class.java)
            startActivityForResult(intent,REQUEST_SEARCH_SUPPLIER)
            return@setOnClickListener
        }
        clear.setOnClickListener() {
            clear()
            return@setOnClickListener
        }
        search.setOnClickListener() {
            search()
            return@setOnClickListener
        }


        var fromCalendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        workingDateFrom.setText(sdf.format(Date()))
        val workingDateFromc = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            fromCalendar.set(Calendar.YEAR, year)
            fromCalendar.set(Calendar.MONTH, monthOfYear)
            fromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //set value at here

            workingDateFrom.setText(sdf.format(fromCalendar.time))
        }

        workingDateTo.setText(sdf.format(Date()))
        var toCalendar = Calendar.getInstance()
        val workingDateToc = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            toCalendar.set(Calendar.YEAR, year)
            toCalendar.set(Calendar.MONTH, monthOfYear)
            toCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            workingDateTo.setText(sdf.format(toCalendar.time))
        }
        workingDateFrom.setOnClickListener {
            DatePickerDialog(this, workingDateFromc, fromCalendar
                    .get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH),
                    fromCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
        workingDateTo.setOnClickListener {
            DatePickerDialog(this, workingDateToc, toCalendar
                    .get(Calendar.YEAR), toCalendar.get(Calendar.MONTH),
                    toCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
        if(SystemInfo.user.user_flag == 5 ||SystemInfo.user.user_flag == 6||
            SystemInfo.user.user_flag == 7||SystemInfo.user.user_flag == 8) {
            lbSection.visibility = View.INVISIBLE
            sectionId.visibility = View.INVISIBLE
            sectionName.visibility = View.INVISIBLE
            searchSection.visibility = View.INVISIBLE
            lbStaff.visibility = View.INVISIBLE
            staffId.visibility = View.INVISIBLE
            staffName.visibility = View.INVISIBLE
            searchStaff.visibility = View.INVISIBLE
            lbSupplier.visibility = View.INVISIBLE
            supplierId.visibility = View.INVISIBLE
            supplierName.visibility = View.INVISIBLE
            searchSupplier.visibility = View.INVISIBLE
            adminCondition.visibility = View.GONE

        }
        search()
    }
    fun  clear() {
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        workingDateFrom.setText(sdf.format(Date()))
        workingDateTo.setText(sdf.format(Date()))
        locationAreaName.setText("")
        locationAreaId.setText("")
        ttsOrderId.setText("")
        sectionId.setText("")
        sectionName.setText("")
        staffId.setText("")
        staffName.setText("")
        supplierId.setText("")
        supplierName.setText("")
    }
    fun search() {
        data = mutableListOf()
        viewAdapter = RequestListAdapter(this@S1021Activity, data)
        detailView!!.adapter = viewAdapter
        page = 1
        loading = true
        viewAdapter!!.addLoadingView()
        data = mutableListOf()
        val repository = RequestRepositoryProvider.getRequestRepository()
        var call: Call<ResponseBody> = repository.getRequest(workingDateFrom.text.toString(), workingDateTo.text.toString(),
                locationAreaName.text.toString(), locationAreaId.text.toString(),
                ttsOrderId.text.toString(), sectionId.text.toString(),
                staffId.text.toString(), supplierId.text.toString(), page.toString())
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetRequestResult = Gson().fromJson(body, GetRequestResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    viewAdapter!!.removeLoadingView()
                    val formatter:DecimalFormat = DecimalFormat("#,###,###")
                    count.text="検索結果：受付"+formatter.format(result.tts_order_count.toInt())+"件、依頼"+formatter.format(result.request_count.toInt())+"件です。"
                    data.addAll(result.data!!)
                    viewAdapter = RequestListAdapter(this@S1021Activity, data)
                    detailView!!.adapter = viewAdapter
                    loading = false
                } else {
                    dialogPopup(this@S1021Activity, MESSAGE_TYPE.ERROR, result.message)
                    viewAdapter!!.removeLoadingView()
                    viewAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }
    private fun loadMoreConstructionData() {
        if(!loading) {
            loading = true
            viewAdapter!!.addLoadingView()
            Handler().postDelayed({
                page = page + 1
                val repository = RequestRepositoryProvider.getRequestRepository()
                var call:Call<ResponseBody> =  repository.getRequest(workingDateFrom.text.toString(),workingDateTo.text.toString(),
                        locationAreaName.text.toString(),locationAreaId.text.toString(),
                        ttsOrderId.text.toString(),sectionId.text.toString(),
                        staffId.text.toString(),supplierId.text.toString(),page.toString())

                call.enqueue(object: Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        var body:String = response.body()!!.string()
                        var result:GetRequestResult = Gson().fromJson(body,GetRequestResult::class.java)
                        Log.d("result",body)
                        if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                            viewAdapter!!.removeLoadingView()
                            data.addAll(result.data!!)
                            viewAdapter!!.notifyDataSetChanged()
                            scrollListener.setLoaded()
                            loading = false
                        }else {
                            dialogPopup(this@S1021Activity,MESSAGE_TYPE.ERROR, result.message)
                            viewAdapter!!.removeLoadingView()
                            viewAdapter!!.notifyDataSetChanged()
                        }
                    }
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    }

                })

            }, 5000)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                REQUEST_SEARCH_SECTION -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val id = data!!.getStringExtra("id")
                        val sName = data!!.getStringExtra("sectionName")
                        // TODO Update your TextView.
                        sectionId.setText(id)
                        sectionName.setText(sName)
                    }
                }
                REQUEST_SEARCH_STAFF -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val sId = data!!.getStringExtra("staffId") // staffId
                        val sName = data!!.getStringExtra("staffName")
                        // TODO Update your TextView.
                        staffId.setText(sId)
                        staffName.text = sName
                    }
                }
                REQUEST_SEARCH_SUPPLIER -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val id = data!!.getStringExtra("id")
                        val name = data!!.getStringExtra("name")
                        // TODO Update your TextView.
                        supplierId.setText(id)
                        supplierName.setText(name)
                    }
                }
                REQUEST_SEARCH_LOCATION_AREA -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val id = data!!.getStringExtra("id")
                        val name = data!!.getStringExtra("name")
                        // TODO Update your TextView.
                        locationAreaId.setText(id)
                        locationAreaName.setText(name)
                    }
                }
            }
    }


}
