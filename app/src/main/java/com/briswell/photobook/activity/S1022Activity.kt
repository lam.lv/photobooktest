package com.briswell.photobook.activity

import android.os.Bundle
import android.os.StrictMode
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import com.briswell.photobook.R
import com.briswell.photobook.adapter.RequestListAdapter
import net.cachapa.expandablelayout.ExpandableLayout
import com.briswell.photobook.adapter.SectionListAdapter
import com.briswell.photobook.api.GetRequestResult
import com.briswell.photobook.api.GetSectionResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.repository.RequestRepositoryProvider
import com.briswell.photobook.util.api.repository.SectionRepositoryProvider
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_photo.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat


/**
 * 担当部署検索
 * 作成者：luc.nt
 * 作成日：20180828
 */

class S1022Activity : BaseActivity() {

    private lateinit var toogle: Button
    private var expandableLayout: ExpandableLayout? = null
    private   var viewAdapter:SectionListAdapter? = null
    private lateinit var sectionsView: RecyclerView
    private lateinit var search:Button
    private lateinit var clear:Button
    private lateinit var close:Button
    private lateinit var hierarchy3:CheckBox
    private lateinit var currentFlag:CheckBox
    private var page = 1
    private var data: MutableList<Section?> = mutableListOf()
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            var policy: StrictMode.ThreadPolicy =  StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_s1022)
        expandableLayout = findViewById(R.id.expand)

        toogle = findViewById(R.id.toogle)
        search = findViewById(R.id.search)
        clear = findViewById(R.id.clear)
        close = findViewById(R.id.close)
        hierarchy3 = findViewById(R.id.hierarchy_3)
        currentFlag = findViewById(R.id.current_flag)
        sectionsView = findViewById(R.id.sections_view)

        hierarchy3.isChecked = true
        currentFlag.isChecked = true
        toogle.setOnClickListener() {
            expandableLayout!!.toggle();
            if(expandableLayout!!.isExpanded) {
                val imgResource = R.drawable.small_circle_arrow_up
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }else {
                val imgResource = R.drawable.small_circle_arrow_down
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }
        }
        close.setOnClickListener() {
            this.finish()
        }
        search.setOnClickListener() {
            search()
        }
        clear.setOnClickListener() {
            clear()
        }
        search()
    }
    fun clear() {
        hierarchy3.isChecked = true
        currentFlag.isChecked = true
    }
    fun search() {
        data = mutableListOf()
        viewAdapter = SectionListAdapter(this, data)
        sectionsView!!.adapter = viewAdapter
        viewAdapter!!.addLoadingView()
        val repository = SectionRepositoryProvider.getSectionRepository()
        var currentFlg = ""
        if(currentFlag.isChecked) {
            currentFlg = "1"
        }
        var call: Call<ResponseBody> =  repository.getSection("3",currentFlg,page.toString())
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetSectionResult = Gson().fromJson(body, GetSectionResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    viewAdapter!!.removeLoadingView()
                    data.addAll(result.data!!)
                    viewAdapter!!.notifyDataSetChanged()
                } else {
                    dialogPopup(this@S1022Activity, MESSAGE_TYPE.ERROR, result.message)
                    viewAdapter!!.removeLoadingView()
                    viewAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })

    }

}
