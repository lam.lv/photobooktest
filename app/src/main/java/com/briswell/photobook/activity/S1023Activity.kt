package com.briswell.photobook.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.briswell.photobook.R
import com.briswell.photobook.SystemInfo
import net.cachapa.expandablelayout.ExpandableLayout
import com.briswell.photobook.adapter.StaffListAdapter
import com.briswell.photobook.api.GetSectionResult
import com.briswell.photobook.api.GetStaffResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.model.SpinnerItem
import com.briswell.photobook.util.ValueUtil
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.model.Staff
import com.briswell.photobook.util.api.repository.SectionRepositoryProvider
import com.briswell.photobook.util.api.repository.StaffRepositoryProvider
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_photo.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * 担当者検索画面
 * 作成者：luc.nt
 * 作成日：20180828
 */

class S1023Activity : BaseActivity() {

    private lateinit var toogle: Button
    private var expandableLayout: ExpandableLayout? = null
    private   var viewAdapter:StaffListAdapter? = null
    private lateinit var staffName:EditText
    private lateinit var spinnerSection:Spinner
    private lateinit var sectionItem:SpinnerItem
    private lateinit var currentFlag:CheckBox
    private lateinit var spinnerApproveAuthority:Spinner
    private lateinit var approveAuthorityItem: SpinnerItem
    private lateinit var staffsView: RecyclerView
    private lateinit var search:Button
    private lateinit var clear:Button
    private lateinit var self:Button
    private lateinit var close:Button
    private lateinit var dataCount:TextView
    private var data: MutableList<Staff?> = mutableListOf()
    private var sections: MutableList<Section?> = mutableListOf()
    private var sectionId:String? = null
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_s1023)
        expandableLayout = findViewById(R.id.expand)

        toogle = findViewById(R.id.toogle)
        staffName = findViewById(R.id.staff_name)
        spinnerSection = findViewById(R.id.section_id)
        currentFlag = findViewById(R.id.current_flag)
        spinnerApproveAuthority = findViewById(R.id.approve_authority)
        search = findViewById(R.id.search)
        clear = findViewById(R.id.clear)
        self = findViewById(R.id.self)
        close = findViewById(R.id.close)
        dataCount = findViewById(R.id.data_count)
        staffsView = findViewById(R.id.staffs_view)
        toogle.setOnClickListener() {
            expandableLayout!!.toggle();
            if(expandableLayout!!.isExpanded) {
                val imgResource = R.drawable.small_circle_arrow_up
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }else {
                val imgResource = R.drawable.small_circle_arrow_down
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }
        }
        close.setOnClickListener() {
            this.finish()
        }
        sectionId = this!!.intent.extras.get("sectionId").toString()
        getSection()
        getApproveAuthority()
        clear()
        search.setOnClickListener() {
            search()
        }
        clear.setOnClickListener(){
            clear()
        }
        self.setOnClickListener() {
            selectSelf()
        }
        search()
    }
    fun getSection() {
        sections = mutableListOf()
        sectionItem = SpinnerItem("","")
        val repository = SectionRepositoryProvider.getSectionRepository()
        var call: Call<ResponseBody> =  repository.getSection("3","1","1")
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetSectionResult = Gson().fromJson(body, GetSectionResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    var SectionItems:ArrayList<SpinnerItem> = arrayListOf()
                    SectionItems.add(SpinnerItem("","----"))
                    for ( item in result.data!!) {
                        SectionItems.add(SpinnerItem(item.id.toString(), item.sectionFullName.toString()))
                    }
                    var sectiondapter:ArrayAdapter<SpinnerItem> = ArrayAdapter<SpinnerItem>(this@S1023Activity, R.layout.spinner_item, SectionItems)
                    spinnerSection!!.adapter =sectiondapter
                    spinnerSection.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                            sectionItem = parent.selectedItem as SpinnerItem
                            sectionId = sectionItem.value
                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {}
                    })
                    spinnerSection.setSelection(0)
                    if(!sectionId!!.equals("")) {
                        SectionItems.forEachIndexed { index, item ->
                            if(item.value ==sectionId ) {
                                spinnerSection.setSelection(index)
                            }
                        }
                    }
                    sectionItem = spinnerSection.selectedItem as SpinnerItem


                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }
    fun getApproveAuthority() {
        val approveAuthorityAdapter = ArrayAdapter(this, R.layout.spinner_item, ValueUtil.getApproveAuthority())
        spinnerApproveAuthority!!.adapter = approveAuthorityAdapter
        spinnerApproveAuthority.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                approveAuthorityItem = parent.selectedItem as SpinnerItem
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerApproveAuthority.setSelection(0)
        approveAuthorityItem = spinnerApproveAuthority.selectedItem as SpinnerItem
    }
    fun search() {
        data = mutableListOf()
        viewAdapter = StaffListAdapter(this, data)
        staffsView!!.adapter = viewAdapter
        dataCount.setText("0件みつかりました。")
        viewAdapter!!.addLoadingView()
        val repository = StaffRepositoryProvider.getStaffRepository()

        var curentFlag:String = ""
        if(currentFlag.isChecked) {
            curentFlag = "1"
        }

        var call: Call<ResponseBody> =  repository.getStaff(staffName.text!!.toString(),sectionId,curentFlag,approveAuthorityItem!!.value.toString())
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetStaffResult = Gson().fromJson(body, GetStaffResult::class.java)
                Log.d("result", body)

                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    viewAdapter!!.removeLoadingView()
                    data.addAll(result.data!!)
                    dataCount.setText(result.count.toString()+"件みつかりました。")
                    viewAdapter!!.notifyDataSetChanged()
                } else {
                    dialogPopup(this@S1023Activity, MESSAGE_TYPE.ERROR, result.message)
                    viewAdapter!!.removeLoadingView()
                    viewAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })

    }
    fun clear(){
        staffName!!.setText("")
        spinnerSection.setSelection(0)
        spinnerApproveAuthority.setSelection(0)
        spinnerApproveAuthority.setSelection(0)
        currentFlag.isChecked = true
    }
    fun selectSelf() {
        val resultIntent = Intent(this.getApplicationContext(),S1021Activity::class.java)
        if( SystemInfo.user.user_flag == 1 ||
            SystemInfo.user.user_flag == 2 ||
            SystemInfo.user.user_flag == 3 ||
            SystemInfo.user.user_flag == 4 ) {
            resultIntent.putExtra("staffCode", SystemInfo.user.staff_id.toString())
            resultIntent.putExtra("staffName", SystemInfo.user.user_name)
        }else if(SystemInfo.user.user_flag == 5 ||
                SystemInfo.user.user_flag == 6 ||
                SystemInfo.user.user_flag == 7 ||
                SystemInfo.user.user_flag == 8) {
            resultIntent.putExtra("staffCode", SystemInfo.user.supplier_staff_id.toString())
            resultIntent.putExtra("staffName", SystemInfo.user.user_name)
        }

        this.setResult(Activity.RESULT_OK, resultIntent)
        this.finish()
    }


}
