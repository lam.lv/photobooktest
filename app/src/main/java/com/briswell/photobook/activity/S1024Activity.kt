package com.briswell.photobook.activity

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.briswell.photobook.R
import com.briswell.photobook.adapter.SectionListAdapter
import net.cachapa.expandablelayout.ExpandableLayout
import com.briswell.photobook.adapter.SupplierListAdapter
import com.briswell.photobook.api.GetSectionResult
import com.briswell.photobook.api.GetSupplierResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.ValueUtil
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.model.Supplier
import com.briswell.photobook.util.api.repository.SectionRepositoryProvider
import com.briswell.photobook.util.api.repository.SupplierRepositoryProvider
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.ArrayAdapter
import android.widget.AdapterView
import android.widget.Toast
import com.briswell.photobook.api.model.SpinnerItem
import java.text.DecimalFormat


/**
 * 仕入先検索画面
 * 作成者：luc.nt
 * 作成日：20180828
 */

class S1024Activity : BaseActivity() {

    private lateinit var toogle: Button
    private lateinit var toogleDetail: Button
    private var expand: ExpandableLayout? = null
    private var expandDetail: ExpandableLayout? = null
    private   var viewAdapter:SupplierListAdapter? = null
    private lateinit var dataView: RecyclerView
    private lateinit var kana:EditText
    private lateinit var name:EditText
    private lateinit var integrationFlag:CheckBox
    private lateinit var spinnerOvertimeWork:Spinner
    private lateinit var spinnerPrefecture:Spinner
    private lateinit var city:EditText
    private lateinit var spinnerSupplierTts:Spinner
    private lateinit var search:Button
    private lateinit var clear:Button
    private lateinit var first:Button
    private lateinit var prev:Button
    private lateinit var paging:TextView
    private lateinit var next:Button
    private lateinit var last:Button
    private lateinit var close:Button
    private var page = 1
    private var total = 1
    private lateinit var count:TextView
    private lateinit var spinnerPageRecords:Spinner
    private var pageRecord:SpinnerItem?=null
    private var prefecture:SpinnerItem? = null
    private var supplierTts:SpinnerItem?= null
    private var overTimeWork:SpinnerItem?= null
    private var data: MutableList<Supplier?> = mutableListOf()
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_s1024)
        expand = findViewById(R.id.expand)
        expandDetail = findViewById(R.id.expand_detail)
        toogle = findViewById(R.id.toogle)
        toogleDetail = findViewById(R.id.toogle_detail)
        kana = findViewById(R.id.kana)
        name = findViewById(R.id.name)
        integrationFlag = findViewById(R.id.integration_flag)
        spinnerOvertimeWork = findViewById(R.id.overtime_work)
        spinnerPrefecture = findViewById(R.id.prefecture)
        spinnerPageRecords = findViewById(R.id.page_records)
        city = findViewById(R.id.city)
        spinnerSupplierTts = findViewById(R.id.supplier_tts)
        search = findViewById(R.id.search)
        clear = findViewById(R.id.clear)
        first = findViewById(R.id.first)
        prev = findViewById(R.id.prev)
        paging = findViewById(R.id.paging)
        next = findViewById(R.id.next)
        last = findViewById(R.id.last)
        close = findViewById(R.id.close)
        count = findViewById(R.id.count)
        dataView = findViewById(R.id.data_view)

        initPrefectures()

        initSuppliers()

        intOvertimeWorks()

        initPageNums()

        toogle.setOnClickListener() {
            expand!!.toggle();
            if(expand!!.isExpanded) {
                val imgResource = R.drawable.small_circle_arrow_up
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }else {
                val imgResource = R.drawable.small_circle_arrow_down
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }
        }
        close.setOnClickListener() {
            this.finish()
        }
        toogleDetail.setOnClickListener() {
            expandDetail!!.toggle();
            if(expandDetail!!.isExpanded) {
                val imgResource = R.drawable.small_circle_arrow_up
                toogleDetail.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }else {
                val imgResource = R.drawable.small_circle_arrow_down
                toogleDetail.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }
        }
        search.setOnClickListener() {
            page=1
            search()
            return@setOnClickListener
        }
        clear.setOnClickListener() {
            clear()
        }
        first.setOnClickListener(){
            fist()
            return@setOnClickListener
        }
        prev.setOnClickListener(){
            prev()
            return@setOnClickListener
        }
        next.setOnClickListener(){
            next()
            return@setOnClickListener
        }
        last.setOnClickListener(){
            last()
            return@setOnClickListener
        }
        search()
    }
    fun initPrefectures() {
        val prefectureAdapter = ArrayAdapter<SpinnerItem>(this, R.layout.spinner_item, ValueUtil.getPrefectureItems())
        spinnerPrefecture.adapter =prefectureAdapter
        spinnerPrefecture.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                prefecture = (parent.selectedItem as SpinnerItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerPrefecture.setSelection(0)
        prefecture = spinnerPrefecture.selectedItem as SpinnerItem
    }
    fun initSuppliers() {
        val supplierAdapter = ArrayAdapter(this, R.layout.spinner_item, ValueUtil.getSuppliersTts())
        spinnerSupplierTts.adapter = supplierAdapter
        spinnerSupplierTts.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                supplierTts = parent.selectedItem as SpinnerItem
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerSupplierTts.setSelection(0)
        supplierTts = spinnerSupplierTts.selectedItem as SpinnerItem
    }
    fun intOvertimeWorks() {
        val overTimeWorksAdapter = ArrayAdapter(this, R.layout.spinner_item, ValueUtil.getOverTimeWorks())
        spinnerOvertimeWork.adapter = overTimeWorksAdapter
        spinnerOvertimeWork.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                overTimeWork = parent.selectedItem as SpinnerItem
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerOvertimeWork.setSelection(0)
        overTimeWork = spinnerOvertimeWork.selectedItem as SpinnerItem
    }
   fun initPageNums() {
        val pageRecordsAdapter = ArrayAdapter(this, R.layout.spinner_item, ValueUtil.getPageRecords())
        spinnerPageRecords.adapter = pageRecordsAdapter
        spinnerPageRecords.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                pageRecord = parent.selectedItem as SpinnerItem
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerPageRecords.setSelection(1)
        pageRecord = spinnerPageRecords.selectedItem as SpinnerItem
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

    }
    fun clear() {
        page=1
        kana.setText("")
        name.setText("")
        integrationFlag.isChecked = false
        spinnerOvertimeWork.setSelection(0)
        spinnerPrefecture.setSelection(0)
        spinnerPageRecords.setSelection(1)
        city.setText("")
        spinnerSupplierTts.setSelection(0)
    }
    fun next(){
        if(page< total) {
            page = page+1
        }
        search()
    }
    fun prev() {
        if(page>1) {
            page = page- 1
        }
        search()
    }
    fun fist() {
        page = 1
        search()
    }
    fun last() {
        page = total
        search()
    }
    fun setPaging() {
        val formatter: DecimalFormat = DecimalFormat("#,###,###")
        paging.setText(formatter.format(page)+"/"+formatter.format(total))
    }

    fun search() {
        data = mutableListOf()
        viewAdapter = SupplierListAdapter(this, data)
        dataView!!.adapter = viewAdapter
        viewAdapter!!.addLoadingView()
        val repository = SupplierRepositoryProvider.getSupplierRepository()

        var inter:String = ""
        if(integrationFlag.isChecked) {
            inter = "1"
        }


        var call: Call<ResponseBody> =  repository.getSupplier(
                kana.text.toString(),name.text.toString(),
                inter,overTimeWork!!.value,prefecture!!.value,city.text.toString(),supplierTts!!.value,page.toString(),pageRecord!!.value)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetSupplierResult = Gson().fromJson(body, GetSupplierResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    viewAdapter!!.removeLoadingView()
                    if(result.count.toInt() % pageRecord!!.value.toInt() != 0) {
                        total = (result.count.toInt() / pageRecord!!.value.toInt()) +1
                    }else {
                        total = result.count.toInt() / pageRecord!!.value.toInt()
                    }
                    setPaging()

                    val formatter: DecimalFormat = DecimalFormat("#,###,###")
                    count.setText(formatter.format(result.count.toInt()) + "件見つかりました。")
                    data.addAll(result.data!!)
                    viewAdapter!!.notifyDataSetChanged()
                } else {
                    dialogPopup(this@S1024Activity, MESSAGE_TYPE.ERROR, result.message)
                    viewAdapter!!.removeLoadingView()
                    viewAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })

    }


}
