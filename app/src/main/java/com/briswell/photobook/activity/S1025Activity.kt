package com.briswell.photobook.activity

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.briswell.photobook.R
import net.cachapa.expandablelayout.ExpandableLayout
import com.briswell.photobook.adapter.*
import com.briswell.photobook.api.GetLocationAreaResult
import com.briswell.photobook.api.GetSupplierResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.model.SpinnerItem
import com.briswell.photobook.util.ValueUtil
import com.briswell.photobook.util.api.model.*
import com.briswell.photobook.util.api.repository.LocationAreaRepositoryProvider
import com.briswell.photobook.util.api.repository.SupplierRepositoryProvider
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat


/**
 * 新規アカウント登録画面
 * 作成者：luc.nt
 * 作成日：20180828
 */

class S1025Activity : BaseActivity() {

    private lateinit var toogle: Button
    private var expand: ExpandableLayout? = null
    private   var viewAdapter:LocationAreaListAdapter? = null
    private lateinit var dataView: RecyclerView
    private lateinit var search:Button
    private lateinit var clear:Button
    private lateinit var kana:EditText
    private lateinit var name:EditText
    private lateinit var spinnerMaintenanceFlag: Spinner
    private lateinit var spinnerPrefecture: Spinner
    private lateinit var spinnerPageRecords:Spinner
    private lateinit var city:EditText
    private lateinit var address1:EditText
    private lateinit var first:Button
    private lateinit var prev:Button
    private lateinit var paging: TextView
    private lateinit var next:Button
    private lateinit var last:Button
    private lateinit var close:Button
    private var page = 1
    private var total = 1
    private lateinit var count:TextView
    private var pageRecord: SpinnerItem?=null
    private var prefecture:SpinnerItem? = null
    private var maintenanceFlag:SpinnerItem? = null
    private var data: MutableList<LocationArea?> = mutableListOf()
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_s1025)
        expand = findViewById(R.id.expand)
        kana = findViewById(R.id.kana)
        name = findViewById(R.id.name)
        spinnerMaintenanceFlag = findViewById(R.id.maintenance_flag)
        spinnerPrefecture = findViewById(R.id.prefecture)
        spinnerPageRecords = findViewById(R.id.page_records)
        city = findViewById(R.id.city)
        address1 = findViewById(R.id.address1)
        toogle = findViewById(R.id.toogle)
        search = findViewById(R.id.search)
        clear = findViewById(R.id.clear)
        first = findViewById(R.id.first)
        prev = findViewById(R.id.prev)
        paging = findViewById(R.id.paging)
        next = findViewById(R.id.next)
        last = findViewById(R.id.last)
        close = findViewById(R.id.close)
        count = findViewById(R.id.count)
        dataView = findViewById(R.id.data_view)
        toogle.setOnClickListener() {
            expand!!.toggle();
            if(expand!!.isExpanded) {
                val imgResource = R.drawable.small_circle_arrow_up
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }else {
                val imgResource = R.drawable.small_circle_arrow_down
                toogle.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0)
            }
        }
        close.setOnClickListener() {
            this.finish()
        }
        initPrefectures()
        initMaintenanceFlags()
        initPageNums()
        search.setOnClickListener() {
            page=1
            search()
            return@setOnClickListener
        }
        clear.setOnClickListener() {
            clear()
            return@setOnClickListener
        }
        first.setOnClickListener(){
            fist()
            return@setOnClickListener
        }
        prev.setOnClickListener(){
            prev()
            return@setOnClickListener
        }
        next.setOnClickListener(){
            next()
            return@setOnClickListener
        }
        last.setOnClickListener(){
            last()
            return@setOnClickListener
        }

        search()
    }
    fun initPrefectures() {
        val prefectureAdapter = ArrayAdapter<SpinnerItem>(this, R.layout.spinner_item, ValueUtil.getPrefectureItems())
        spinnerPrefecture.adapter =prefectureAdapter
        spinnerPrefecture.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                prefecture = (parent.selectedItem as SpinnerItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerPrefecture.setSelection(0)
        prefecture = spinnerPrefecture.selectedItem as SpinnerItem
    }
    fun initMaintenanceFlags() {
        val adapter = ArrayAdapter<SpinnerItem>(this, R.layout.spinner_item, ValueUtil.getMaintenanceFlags())
        spinnerMaintenanceFlag.adapter =adapter
        spinnerMaintenanceFlag.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                maintenanceFlag = (parent.selectedItem as SpinnerItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerMaintenanceFlag.setSelection(0)
        maintenanceFlag = spinnerMaintenanceFlag.selectedItem as SpinnerItem
    }
    fun initPageNums() {
        val pageRecordsAdapter = ArrayAdapter(this, R.layout.spinner_item, ValueUtil.getPageRecords())
        spinnerPageRecords.adapter = pageRecordsAdapter
        spinnerPageRecords.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                pageRecord = parent.selectedItem as SpinnerItem
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerPageRecords.setSelection(1)
        pageRecord = spinnerPageRecords.selectedItem as SpinnerItem
    }
    fun search() {
        data = mutableListOf()
        viewAdapter = LocationAreaListAdapter(this, data)
        dataView!!.adapter = viewAdapter
        viewAdapter!!.addLoadingView()
        val repository = LocationAreaRepositoryProvider.getLocationAreaRepository()




        var call: Call<ResponseBody> =  repository.getLocationArea(
                 kana.text.toString(),name.text.toString(),maintenanceFlag!!.value
                ,prefecture!!.value,city.text.toString(),address1!!.text.toString()
                ,page.toString(),pageRecord!!.value)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetLocationAreaResult = Gson().fromJson(body, GetLocationAreaResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    viewAdapter!!.removeLoadingView()
                    if(result.count.toInt() % pageRecord!!.value.toInt() != 0) {
                        total = (result.count.toInt() / pageRecord!!.value.toInt()) +1
                    }else {
                        total = result.count.toInt() / pageRecord!!.value.toInt()
                    }
                    setPaging()

                    val formatter: DecimalFormat = DecimalFormat("#,###,###")
                    count.setText(formatter.format(result.count.toInt()) + "件見つかりました。")
                    data.addAll(result.data!!)
                    viewAdapter!!.notifyDataSetChanged()
                } else {
                    dialogPopup(this@S1025Activity, MESSAGE_TYPE.ERROR, result.message)
                    viewAdapter!!.removeLoadingView()
                    viewAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })

    }
    fun setPaging() {
        val formatter: DecimalFormat = DecimalFormat("#,###,###")
        paging.setText(formatter.format(page)+"/"+formatter.format(total))
    }
    fun next(){
        if(page< total) {
            page = page+1
        }
        search()
    }
    fun prev() {
        if(page>1) {
            page = page- 1
        }
        search()
    }
    fun fist() {
        page = 1
        search()
    }
    fun last() {
        page = total
        search()
    }

    fun clear() {
        page=1
        kana.setText("")
        name.setText("")
        spinnerMaintenanceFlag.setSelection(0)
        spinnerPrefecture.setSelection(0)
        city.setText("")
        address1.setText("")
        spinnerPageRecords.setSelection(1)
    }
}
