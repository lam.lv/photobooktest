package com.briswell.photobook.activity

import android.Manifest
import android.app.*
import android.content.*
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.*
import android.util.Log
import android.view.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper

import com.briswell.photobook.util.api.repository.RequestRepositoryProvider
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import android.widget.Toast
import com.briswell.photobook.BuildConfig
import com.briswell.photobook.R
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.adapter.ReportPhotoAdapter
import com.briswell.photobook.adapter.ReportPhotoDetailAdapter
import com.briswell.photobook.api.GetReportPhotoResult
import com.briswell.photobook.api.GetTemplateDetailResult
import com.briswell.photobook.api.GetTemplateResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.model.ReportPhoto
import com.briswell.photobook.api.model.ReportPhotoDetail
import com.briswell.photobook.api.model.SpinnerItem
import com.briswell.photobook.api.repository.GetReportPhotoFileResult
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.dialog.InputDirectoryNameDialog
import com.briswell.photobook.dialog.S1031DialogReportActivity
import com.briswell.photobook.helper.*
import com.briswell.photobook.model.IDragDropModel
import com.briswell.photobook.model.SendBroadcastReceiverModel
import com.briswell.photobook.receiver.DownloadCompleteReceiver
import com.briswell.photobook.util.*
import com.briswell.photobook.util.Constant.VIEW_TYPE_ITEM
import com.briswell.photobook.util.Constant.VIEW_TYPE_LOADING
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.google.gson.reflect.TypeToken
import com.itextpdf.text.*

import com.itextpdf.text.pdf.*
import kotlinx.android.synthetic.main.activity_s1031.*
import okhttp3.ResponseBody
import org.jetbrains.anko.sdk25.coroutines.onClick
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList


class S1031Activity : BaseActivity(), Listener, OnStartDragListener {

    internal var mDrawerToggle: ActionBarDrawerToggle? = null
    internal var reportPhoto: ReportPhoto? = null
    internal var reportPhotos: MutableList<IDragDropModel?>? = mutableListOf()
    internal var reportPhotoDetails: MutableList<IDragDropModel?> = mutableListOf()
    internal var reportPhotoDetailsDeleted:MutableList<IDragDropModel?> = mutableListOf()
    internal var reportPhotoDetailsBak: MutableList<IDragDropModel?> = mutableListOf()
    private lateinit var detailView: RecyclerView
    private lateinit var photoView: RecyclerView
    private lateinit var reportPhotoFileAdapter: ReportPhotoAdapter
    private lateinit var reportPhotoDetailAdapter: ReportPhotoDetailAdapter
    private lateinit var btnMenu: LinearLayout
    private lateinit var photoLayout: RelativeLayout
    private lateinit var controlGroupLayout: LinearLayout
    private lateinit var btnUpload: LinearLayout
    private lateinit var btnDownload:LinearLayout
    private lateinit var btnClose: Button
    private lateinit var checkAll: LinearLayout
    private lateinit var apply: LinearLayout
    private lateinit var spinnerTemplates:Spinner
    private var template: SpinnerItem? = null
    private lateinit var registReport: Button
    private lateinit var regist: Button
    private lateinit var btnDelete: Button
    private lateinit var btnAddRow: Button
    private lateinit var btnCreateDirectory: Button
    private lateinit var btnBack:Button
    private lateinit var uploadStatusBar: ProgressBar
    private lateinit var zoomin: ImageView
    private lateinit var zoomout: ImageView
    private lateinit var leftLayout: LinearLayout
    private lateinit var applyTemplate: Button
    private lateinit var sheet3:RadioButton
    private lateinit var sheet6:RadioButton
    private var offset: Int = 0
    private var limit: Int = 20
    private var dataClone: MutableList<IDragDropModel?> = mutableListOf()
    var newfile: File? = null
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var layoutManager: GridLayoutManager? = null
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private val REQUEST_CAMERA = 100
    private val REQUEST_FILE = 300
    private val REQUEST_PERMISSION = 200
    private val REQUEST_REGIST_REPORT_PHOTO = 400
    private val CHANNEL_ID = "DOWNLOAD_REPORT_PHOTO"
    var NOTIFICATION_ID = 1100
    var col: Int = 3
    var requestId = ""
    var filePath = ""
    var directoryId = ""
    var picturePath = ""
    private var checkedAll = false
    var downloadId:Long? = null
    lateinit var node:IDragDropModel
    var downloadComplete:DownloadCompleteReceiver?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_s1031)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            var policy: StrictMode.ThreadPolicy = StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        if (!checkPermission()) {
            requestPermission()
        }
        photoView = findViewById(R.id.photo_list)
        detailView = findViewById(R.id.detail_list)
        detailView!!.setLayoutManager(LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false))
        regist = findViewById(R.id.btn_regist)
        btnMenu = findViewById(R.id.btnMenu)
        btnUpload = findViewById(R.id.btnUpload)
        btnCreateDirectory = findViewById(R.id.create_directory)
        btnClose = findViewById<Button>(R.id.btnClose)
        checkAll = findViewById(R.id.checkAll)
        apply = findViewById(R.id.apply)
        registReport = findViewById(R.id.regist_report)
        zoomin = findViewById(R.id.zoomin)
        zoomout = findViewById(R.id.zoomout)
        applyTemplate = findViewById(R.id.apply_template)
        photoLayout = findViewById<RelativeLayout>(R.id.photo_layout)
        controlGroupLayout = findViewById(R.id.control_group)
        btnDelete = findViewById(R.id.btn_delete)
        btnAddRow = findViewById(R.id.btn_add_row)
        leftLayout = findViewById(R.id.left_layout)
        uploadStatusBar = findViewById(R.id.upload_status_bar)
        spinnerTemplates = findViewById(R.id.templates)
        btnBack = findViewById(R.id.back)
        btnDownload = findViewById(R.id.download)
        sheet3 = findViewById(R.id.sheet3)
        sheet6 = findViewById(R.id.sheet6)

        requestId = this!!.intent.extras.get("request_id").toString()
        uploadStatusBar.progress = 0
        uploadStatusBar.visibility = View.GONE
        node = IDragDropModel(checked = false,id = null,file_path = "",
                file_name = "",thumbnail_file_path = "",report_photo_file_id = 0,
                isDirectory = false,display = "",comment = "",manual = "",
                upload_status = 0,parent = null,childrens = null,useNum = 0,checkNum = 0)
        getReportPhotoFiles()
        getReportPhoto()
        initTemplates()
        //メニューボタン
        btnMenu.setOnClickListener {
            menuClick()
        }
        //アップロードボタン
        btnUpload.setOnClickListener {
            upload()
        }
        btnCreateDirectory.setOnClickListener() {
            createDirectory()
        }
        btnClose.setOnClickListener {
            if(isChanged()) {
                this.showMessageOKCancel(resources.getString(R.string.CFRM_004),
                        DialogInterface.OnClickListener { dialog, which ->
                            this.finish()
                        })
            }else {
                this.finish()
            }

        }


        checkAll.setOnClickListener {
            checkedAll = !checkedAll
            for (item in reportPhotos!!) {
                item!!.checked = checkedAll
            }
            reportPhotoFileAdapter.updateDataSet(reportPhotos!!)
        }

        apply.setOnClickListener {
            var list: MutableList<IDragDropModel?> = mutableListOf()
            var max = 0
            for(item in reportPhotos!!) {
                if(item!!.checkNum>max)
                {
                    max = item!!.checkNum
                }
            }
            for( i in 0..max) {
                for (item in reportPhotos!!) {
                    if (item!!.checked == true && item!!.checkNum == i) {
                        list.add(item)
                    }
                }
            }
            for (item in reportPhotos!!) {
                item!!.checked = false
                item!!.checkNum = 0
            }
            reportPhotoDetails.addAll(list)
            reportPhotoDetailAdapter.notifyItemRangeInserted(reportPhotoDetails.size-list.size,list.size)
            resetUseNum()
        }
        regist.setOnClickListener {
            if (validate()) {
                regist()
            }
        }
        registReport.setOnClickListener() {
            val intent = Intent(getApplicationContext(), S1031DialogReportActivity::class.java)
            intent.putExtra("report_photo", Gson().toJson(reportPhoto))
            startActivityForResult(intent, REQUEST_REGIST_REPORT_PHOTO)
            return@setOnClickListener
        }
        btnDelete.setOnClickListener() {
            this.showMessageOKCancel(getResources().getString(R.string.CFRM_003),
                    DialogInterface.OnClickListener { dialog, which ->
                        deleteMutilpleRow()
                        resetUseNum()
                    })

        }
        btnAddRow.setOnClickListener() {
            var checkedCnt = 0
            var checkedIdx = 0
            reportPhotoDetails.forEachIndexed { index, item ->
                if(item!!.checked!!) {
                    checkedCnt ++
                    checkedIdx = index
                }
            }
            var newItem = IDragDropModel(checked = false, id = 0, report_photo_file_id = 0, file_path = "", file_name = "", thumbnail_file_path = "",
                    comment = "", manual = "", upload_status = 0, parent = null, childrens = null, isDirectory = false,display = null,useNum = 0,checkNum = 0)
            if(checkedCnt == 1) {
                reportPhotoDetails.add(checkedIdx+1,newItem)
                reportPhotoDetailAdapter.notifyItemInserted(checkedIdx+1)
            }else {
                reportPhotoDetails.add(newItem)
                reportPhotoDetailAdapter.notifyItemInserted(reportPhotoDetails.count())
            }


        }

        applyTemplate.setOnClickListener() {
            applyTemplate()
        }

        layoutManager = (photoView.layoutManager as GridLayoutManager)
        layoutManager!!.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (reportPhotoFileAdapter.getItemViewType(position)) {
                    VIEW_TYPE_ITEM -> return 1
                    VIEW_TYPE_LOADING -> return col //number of columns of the grid
                    else -> return -1
                }
            }
        })
        scrollListener = RecyclerViewLoadMoreScroll(layoutManager!!)
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (!loading) {
                    Log.d("IJNET", "start load more")
                }
            }
        })

        photoView.addOnScrollListener(scrollListener)
        zoomin.onClick {
            zoomin()
        }
        zoomout.onClick {
            zoomout()
        }
        btnBack.setOnClickListener() {
            if(node.parent != null) {
                this.node = node.parent!!
                getReportPhotoFiles()
            }
        }
        btnDownload.setOnClickListener() {
            download()
        }
        downloadComplete = DownloadCompleteReceiver()
        registerReceiver(downloadComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(downloadComplete);
    }
    fun isChanged():Boolean {
        if(reportPhotoDetails.size != reportPhotoDetailsBak.size)
        {
            return true
        }
        if(reportPhotoDetails.size == reportPhotoDetailsBak.size) {
            var i:Int = 0
            for(it in reportPhotoDetails)
            {
                if(it!!.comment !=reportPhotoDetailsBak[i]!!.comment ||
                        it!!.manual !=reportPhotoDetailsBak[i]!!.manual||
                        it!!.report_photo_file_id !=reportPhotoDetailsBak[i]!!.report_photo_file_id) {
                    return true
                }
                i += 1
            }
        }
        return false
    }
    fun download() {
        val df = SimpleDateFormat("yyyyMMddHHmmss")
        val date = Date()
        var fileName = df.format(date) + ".zip"
        var passPhrase = CryptoUtil.sha256Encrypt("download_report_photo")
        var requestUrl = BuildConfig.apiUrl+"download_report_photo?apikey="+BuildConfig.apiKey+"&passphrase="+passPhrase+"&version="+BuildConfig.version+"&request_id="+requestId
        //http://tts-photobook-develop-1304357941.ap-northeast-1.elb.amazonaws.com/photobook/api/v1/download_report_photo?apikey=YxGRMwvMRaAkCdZdBv0sCIyCTQL1KR4e&passphrase=ea63567239f7de65394a4c35dd5a682fb24522cdf4f24eac106bced00932d5c5&version=1.0&request_id=806393
        var uri: Uri = Uri.parse(requestUrl)
        var request: DownloadManager.Request = DownloadManager.Request(uri)
        request.addRequestHeader("Cookie", IJNetApiService.REQUEST_COOKIES)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setTitle("写真帳アプリ")
        request.setVisibleInDownloadsUi(true)
        request.setMimeType("application/zip")
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
        downloadId = (getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager).enqueue(request)
        downloadComplete!!.downloadId = downloadId
    }

    fun onRadioTemplateClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.sheet3 ->
                    if (checked) {
                        reportPhoto!!.template_type = "3"

                    }
                R.id.sheet6 ->
                    if (checked) {
                        reportPhoto!!.template_type = "6"
                    }
            }
            reportPhotoDetailAdapter.notifyItemRangeChanged(0,reportPhotoDetailAdapter.dataset!!.size,true)

        }
    }
    fun initTemplates() {
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var templates :ArrayList<SpinnerItem> = arrayListOf()
        var call: Call<ResponseBody> = repository.getTemplate()
        var body: String = call.execute().body()!!.string()
        var result: GetTemplateResult = Gson().fromJson(body, GetTemplateResult::class.java)
        if (result.status == IJNetApiService.ApiStatus.OK.value) {
            templates.add(SpinnerItem("0", "----"))
            for (item in result.data!!) {
                templates.add(SpinnerItem(item.id, item.template_name))
            }

        }
        val tempalteAdapter = ArrayAdapter<SpinnerItem>(this, R.layout.spinner_item, templates)

        spinnerTemplates.adapter =tempalteAdapter
        spinnerTemplates.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                template = (parent.selectedItem as SpinnerItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        })
        spinnerTemplates.setSelection(0)
        template = spinnerTemplates.selectedItem as SpinnerItem

    }
    fun applyTemplate() {
        var act: BaseActivity = BaseActivity()
        if(template == null || template!!.value == "" ||  template!!.value == "0"){
            act.dialogPopup(this, BaseActivity.MESSAGE_TYPE.ERROR, resources.getString(R.string.CERR_008))
            return
        }
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var call: Call<ResponseBody> = repository.getTemplateDetail(template!!.value)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetTemplateDetailResult = Gson().fromJson(body, GetTemplateDetailResult::class.java)
                Log.d("result", body)
                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    if (result.data != null) {
                        for (item in result.data!!) {
                            reportPhotoDetails.add(IDragDropModel(checked = false,id = 0,file_path = "",file_name = "",
                                    thumbnail_file_path = "",upload_status = 0,comment = item.comment,manual = item.information,
                                    isDirectory = false,parent = null,childrens = null,report_photo_file_id = null,display = "",useNum = 0,checkNum = 0))
                            reportPhotoDetailAdapter.notifyItemInserted(reportPhotoDetails.count()-1)
                        }


                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }
    fun createDirectory() {
        var dialog: InputDirectoryNameDialog = InputDirectoryNameDialog(this, object : InputDirectoryNameDialog.InputDirectoryNameDialogListener {
            override fun onOK(name: String) {
                val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
                var call: Call<ResponseBody> = repository.setReportPhotoFile(requestId, name, "1", SystemInfo.user.user_id, directoryId, null)
                var body: String = call.execute().body()!!.string()
                var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
                if (result.status == IJNetApiService.ApiStatus.OK.value) {
                    reportPhotos!!.add(IDragDropModel(id = 0, checked = false, file_path = result.data!!.file_path,
                            file_name = name,
                            thumbnail_file_path = result.data!!.file_path,
                            comment = "", manual = "", upload_status = 0,
                            report_photo_file_id = result.data!!.id.toLong(), childrens = null, parent = null, isDirectory = true,display = null,useNum = 0,checkNum = 0))
                }
                reportPhotoFileAdapter.updateDataSet(reportPhotos!!);
            }

            override fun onCancel() {

            }
        },false)
        dialog.show()
    }

    fun resetUseNum() {
        var useNum = 0
        for(photo in reportPhotos!!) {
            useNum = 0
            for(dt in reportPhotoDetails) {
                if(photo!!.report_photo_file_id == dt!!.report_photo_file_id) {
                    useNum ++
                }
            }
            photo!!.useNum = useNum
        }
        reportPhotoFileAdapter.updateDataSet(reportPhotos!!)
    }
    fun getReportPhotoFiles() {
        reportPhotos = mutableListOf()
        reportPhotoFileAdapter = ReportPhotoAdapter(this@S1031Activity, photoView, reportPhotos!!, this@S1031Activity, this@S1031Activity)
        photoView!!.adapter = reportPhotoFileAdapter
        photoView!!.setOnDragListener(reportPhotoFileAdapter!!.dragInstance)
        reportPhotoFileAdapter!!.addLoadingView()


        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var reportPhotoFileId = ""
        if(!node.report_photo_file_id.toString().isNullOrEmpty()) {
            reportPhotoFileId = node.report_photo_file_id.toString()
        }
        var call: Call<ResponseBody> = repository.getReportPhotoFile(requestId, reportPhotoFileId.toString(), "")
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                var result: GetReportPhotoFileResult = Gson().fromJson(body, GetReportPhotoFileResult::class.java)
                Log.d("result", body)

                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    var isDirectory: Boolean = false;
                    if (result.data != null) {
                        for (item in result.data!!) {
                            isDirectory = false
                            if (item.folder_flag != null && !item.folder_flag.trim().equals("") && item.folder_flag.toInt() == 1) {
                                isDirectory = true;
                            }
                            reportPhotos!!.add(IDragDropModel(id = 0, checked = false, file_path = item.file_path,
                                    file_name = getFileNameFromUrl(item.file_path),
                                    thumbnail_file_path = item.file_path,
                                    comment = "", manual = "", upload_status = 0,
                                    report_photo_file_id = item.id.toLong(), childrens = null, parent = null, isDirectory = isDirectory,display = "",useNum = 0,checkNum = 0))
                        }

                    }
                    node.childrens = reportPhotos!!
                    reportPhotoFileAdapter.notifyDataSetChanged()

                    try {
                        resetUseNum()
                    }catch (e:Exception) {}
                    reportPhotoFileAdapter!!.removeLoadingView()
                    reportPhotoFileAdapter!!.notifyDataSetChanged()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }

    fun getFilePathFromUrl(url: String): String {
        var remove = BuildConfig.awsCloudFrontUrl + "/" + BuildConfig.awsAppDir + "/" + requestId + "/"
        var path = url.replace(remove, "");
        remove = "?" + BuildConfig.awsBucket
        path = path.replace(remove, "")
        return path
    }

    fun getFileNameFromUrl(url: String): String {
        var remove = BuildConfig.awsCloudFrontUrl + "/" + BuildConfig.awsAppDir + "/" + requestId + "/"
        var path = url.replace(remove, "");
        remove = "?" + BuildConfig.awsBucket
        path = path.replace(remove, "")
        var paths: List<String> = path.split("/")
        return paths.get(paths.size - 1)
    }

    fun getReportPhoto() {
        reportPhotoDetails = mutableListOf()
        reportPhotoDetailsBak = mutableListOf()
        reportPhotoDetailAdapter = ReportPhotoDetailAdapter(this@S1031Activity, detailView, reportPhotoDetails, this@S1031Activity, this@S1031Activity)
        detailView!!.adapter = reportPhotoDetailAdapter
        detailView!!.setOnDragListener(reportPhotoDetailAdapter!!.dragInstance)
        reportPhotoDetailAdapter.addLoadingView()
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var call: Call<ResponseBody> = repository.getReportPhoto(requestId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                var body: String = response.body()!!.string()
                Log.d("result", body)
                var result: GetReportPhotoResult = Gson().fromJson(body, GetReportPhotoResult::class.java)

                if (result!!.status == IJNetApiService.ApiStatus.OK.value) {
                    if (result.data != null) {
                        reportPhoto = result.data.report_photo_data
                        if(reportPhoto!!.template_type.toLong() == 3L){
                            sheet3.isChecked = true
                        }else if(reportPhoto!!.template_type.toLong() == 6L) {
                            sheet6.isChecked = true
                        }
                        reportPhoto!!.location_area_id = result.data.request_data!!.location_area_id.toString()
                        if (result.data!!.report_photo_detail_data != null) {
                            var i = 0
                            for (item in result.data!!.report_photo_detail_data!!) {

                                var file_path = ""
                                if(result.data!!.report_photo_file_data != null) {
                                    for (pt in result.data!!.report_photo_file_data!!) {
                                        if (!item.report_photo_file_id.isNullOrEmpty()) {
                                            if (pt.id.toLong() == item.report_photo_file_id!!.toLong()) {
                                                file_path = pt.file_path
                                            }
                                        }

                                    }
                                }

                                var reportPhotoFileId  = 0L
                                if(!item.report_photo_file_id.isNullOrEmpty()) {
                                    reportPhotoFileId = item.report_photo_file_id!!.toLong()
                                }
                                val it = IDragDropModel(id = item.id, checked = false, file_path = file_path,
                                        file_name = getFileNameFromUrl(file_path),
                                        thumbnail_file_path = file_path,
                                        comment = item.comment, manual = item.manual, upload_status = 0,
                                        report_photo_file_id = reportPhotoFileId, childrens = null, parent = null, isDirectory = false,display = "",useNum = 0,checkNum = 0)
                                reportPhotoDetails.add(it)
                                val cp = it.copy()
                                reportPhotoDetailsBak.add(cp)
                                i++
                            }
                            reportPhotoDetailAdapter.notifyItemRangeInserted(reportPhotoDetails.size - result.data!!.report_photo_detail_data!!.size -1,
                                    result.data!!.report_photo_detail_data!!.size)
                        }
                        reportPhotoDetailAdapter!!.removeLoadingView()
                        reportPhotoDetailAdapter!!.notifyDataSetChanged()
                        resetFirstRow()
                        try {
                            resetUseNum()
                        }catch (e:Exception) {}
                   }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

        })
    }


    fun resetFirstRow() {
        if(reportPhotoDetails.count()==0) {
            var it = IDragDropModel(id = 0, checked = false, file_path = "",
                    file_name = "",
                    thumbnail_file_path = "",
                    comment = "表紙", manual = "建物全景写真を貼る。", upload_status = 0,
                    report_photo_file_id = 0, childrens = null, parent = null, isDirectory = false,display = "",useNum = 0,checkNum = 0,isLabel = true)
            reportPhotoDetails.add(it)
            var cp = it.copy()
            reportPhotoDetailsBak.add(cp)
            reportPhotoDetailAdapter.notifyItemInserted(reportPhotoDetails.size -1)
        }else {
            reportPhotoDetails.get(0)!!.comment = "表紙"
            reportPhotoDetails.get(0)!!.manual = "建物全景写真を貼る。"
            reportPhotoDetails.get(0)!!.isLabel = true
            reportPhotoDetailsBak.get(0)!!.comment = "表紙"
            reportPhotoDetailsBak.get(0)!!.manual = "建物全景写真を貼る。"
            reportPhotoDetailsBak.get(0)!!.isLabel = true

        }
    }
    fun deleteMutilpleRow() {
        for ( i in reportPhotoDetails.count()-1 downTo 0) {
            if(reportPhotoDetails[i]!!.checked!!) {
                if(reportPhotoDetails[i]!!.id!!>0) {
                    var it = reportPhotoDetails[i]
                    reportPhotoDetailsDeleted.add(it)
                    reportPhotoDetails.removeAt(i)
                    reportPhotoDetailAdapter.notifyItemRemoved(i)
                }else {
                    reportPhotoDetails.removeAt(i)
                    if(i<=reportPhotoDetailsBak.size-1) {
                        reportPhotoDetailsBak.removeAt(i)
                    }
                    reportPhotoDetailAdapter.notifyItemRemoved(i)
                }
                if(i<reportPhotoDetailAdapter.dataset!!.size) {
                    reportPhotoDetailAdapter.notifyItemRangeChanged(i,reportPhotoDetailAdapter.dataset!!.size -(i))
                }
            }
        }

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        try {
            var size = reportPhotoFileAdapter.getItemSize()
            col = leftLayout.width / size
            if (col <= 6 && col >= 1) {
                reportPhotoFileAdapter.setItemSize(size)
            }
            if (col > 6) {
                col = 6
            } else if (col < 1) {
                col = 1
            }
            layoutManager!!.spanCount = col
        } catch (e: Exception) {

        }
    }

    public fun changed(): Boolean {
        var flg: Boolean = false
        if (dataClone != reportPhotoDetailAdapter.dataset) {
            flg = true
        }
        return flg
    }


    private var loading: Boolean = false

    fun zoomin() {

        var size = reportPhotoFileAdapter.getItemSize();
        size = size + 5
        col = leftLayout.width / size;
        if (col <= 6 && col >= 1) {
            reportPhotoFileAdapter.setItemSize(size)
        }
        if (col > 6) {
            col = 6
        } else if (col < 1) {
            col = 1
        }
        layoutManager!!.spanCount = col

    }

    fun zoomout() {
        var size = reportPhotoFileAdapter.getItemSize()
        size = size - 5
        col = leftLayout.width / size
        if (col <= 6 && col >= 1) {
            reportPhotoFileAdapter.setItemSize(size)
        }
        if (col > 6) {
            col = 6
        } else if (col < 1) {
            col = 1
        }
        layoutManager!!.spanCount = col
    }

    fun createPdf(fileName: String) {
        // step 1

        var document: Document = Document();
        // step 2

        PdfWriter.getInstance(document, FileOutputStream(fileName));
        // step 3
        document.open();
        // step 4
        document.add(createDataTable());
        // step 5
        document.close();


    }

    fun openPdf(fileName: String) {

        var file: File = File(fileName);
        var target: Intent = Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        var intent: Intent = Intent.createChooser(target, "Open File");

        startActivity(intent);

    }

    /**
     * Creates our first table
     * @return our first table
     */
    fun createDataTable(): PdfPTable {
        var table: PdfPTable = PdfPTable(3);
        val columnWidths = intArrayOf(100, 150, 150)
        table.setWidths(columnWidths)
        for (i in reportPhotoDetails) {
            var cell: PdfPCell
            cell = createImageCell(i!!.thumbnail_file_path!!)
            cell.fixedHeight = 100f
            cell.paddingTop = 5f

            cell.horizontalAlignment = Element.ALIGN_CENTER
            cell.verticalAlignment = Element.ALIGN_CENTER

            table.addCell(cell)
            table.addCell(i.comment)
            table.addCell(i.manual)
        }
        return table
    }

    fun createImageCell(path: String): PdfPCell {
        var img: Image = Image.getInstance(URL(path));
        img.scaleAbsolute(90f, 90f)
        img.paddingTop = 5f
        img.alignment = Image.ALIGN_CENTER
        var cell: PdfPCell = PdfPCell(img);
        return cell
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper!!.startDrag(viewHolder)

    }


    fun validate(): Boolean {
        return true
    }

    fun regist() {
        var act: BaseActivity = BaseActivity()
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var i = 1
        var success = true
        var id = ""
        var mode = 1
        //データを削除する
        for(it in reportPhotoDetailsDeleted) {
            var call: Call<ResponseBody> = repository.setReportPhotoDetail("3", it!!.id.toString(), reportPhoto!!.id, it!!.report_photo_file_id.toString(), it!!.comment.toString(), it!!.manual, i.toString(), SystemInfo.user.user_id)
            var body: String = call.execute().body()!!.string()
            Log.d("result:", body);
            var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
            if(result.status != IJNetApiService.ApiStatus.OK.value) {
                success = false
            }
        }
        if(success) {
            reportPhotoDetailsDeleted = mutableListOf()
        }
        //データを保存する
        if (!reportPhoto!!.id.isNullOrEmpty() && reportPhoto!!.id.toLong() > 0) {
            mode = 2
            id = reportPhoto!!.id.toString()
        }
        var call: Call<ResponseBody> = repository.setReportPhoto(mode.toString(), id, reportPhoto!!.location_area_id,
                reportPhoto!!.tts_order_id, reportPhoto!!.request_id, reportPhoto!!.template_type, reportPhoto!!.customer_name, reportPhoto!!.title,
                reportPhoto!!.location_name, reportPhoto!!.working_date, reportPhoto!!.invoice_name_suf, SystemInfo.user.user_id)
        var body: String = call.execute().body()!!.string()
        Log.d("result:", body);
        var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)

        if (result.status != IJNetApiService.ApiStatus.OK.value) {
            success = false
            act.dialogPopup(this, BaseActivity.MESSAGE_TYPE.ERROR, resources.getString(R.string.SERR_001))
            return
        }
        if(reportPhoto!!.id.isNullOrEmpty()) {
            var call: Call<ResponseBody> = repository.getReportPhoto(requestId)
            var body: String = call.execute().body()!!.string()
            Log.d("result", body)
            var result: GetReportPhotoResult = Gson().fromJson(body, GetReportPhotoResult::class.java)
            if(result.status ==IJNetApiService.ApiStatus.OK.value) {
                reportPhoto!!.id = result.data.report_photo_data!!.id
            }
        }

        for (detailModel in reportPhotoDetails) {
            var reportPhotoId = ""
            var id = ""
            if (!reportPhoto!!.id.isNullOrEmpty() && reportPhoto!!.id.toLong() > 0) {
                reportPhotoId = reportPhoto!!.id
            }
            var mode = 1
            if (detailModel!!.id!! > 0) {
                mode = 2
                id = detailModel.id.toString()
            }
            var reportPhotoFileId = ""
            if(detailModel.report_photo_file_id != null && detailModel.report_photo_file_id!!>0) {
                reportPhotoFileId = detailModel.report_photo_file_id.toString()
            }
            var call: Call<ResponseBody> = repository.setReportPhotoDetail(mode.toString(), id, reportPhotoId, reportPhotoFileId, detailModel.comment.toString(), detailModel.manual, i.toString(), SystemInfo.user.user_id)
            var body: String = call.execute().body()!!.string()
            Log.d("result:", body);
            var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)

            if (result.status != IJNetApiService.ApiStatus.OK.value) {
                success = false
            }

            i++
        }

        if (success) {
            act.dialogPopup(this, BaseActivity.MESSAGE_TYPE.SUCCESS, resources.getString(R.string.INFO_001))
            getReportPhoto()
            reportPhotoDetailsBak = mutableListOf()
        } else {
            act.dialogPopup(this, BaseActivity.MESSAGE_TYPE.ERROR, resources.getString(R.string.SERR_001))
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        return if (mDrawerToggle!!.onOptionsItemSelected(item)) {
            true
        } else super.onOptionsItemSelected(item)
        // Handle your other action bar items...
    }

    fun menuClick() {
        if (photoLayout.visibility == View.VISIBLE) {
            photoLayout.visibility = View.GONE;
            controlGroupLayout.visibility = View.GONE;
        } else {
            photoLayout.visibility = View.VISIBLE;
            controlGroupLayout.visibility = View.VISIBLE;
        }

        Log.d("TAG", "CLICK.");
    }

    fun upload() {
        try {
            
            val intent = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            startActivityForResult(intent, REQUEST_FILE)


        } catch (e: Exception) {

            e.printStackTrace()

        }
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this!!.applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(this!!.applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (grantResults.size > 0) {

                val write = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val read = grantResults[1] == PackageManager.PERMISSION_GRANTED

                if (write && read) {

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            this.showMessageOKCancel(resources.getString(R.string.CFRM_005),
                                    DialogInterface.OnClickListener { dialog, which ->
                                            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                                                    REQUEST_PERMISSION)

                                    })
                            return
                        }
                    }
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        println("==========requestCode==========" + requestCode)

        if ((requestCode == REQUEST_CAMERA || requestCode == REQUEST_FILE)
                && resultCode == Activity.RESULT_OK
                && resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == REQUEST_CAMERA) {
                // System.out.println("mCapturedImageURI****PICK_FROM_CAMERA*********" + mCapturedImageURI)
                var f = File(Environment.getExternalStorageDirectory()
                        .toString())
                for (temp in f.listFiles()!!) {
                    if (temp.name == "temp.jpg") {
                        f = temp
                        break
                    }
                }
                picturePath = Environment.getExternalStorageDirectory().absolutePath + File.separator + "temp.jpg"
                newfile = File(picturePath)
                //handleSmallCameraPhoto(data);
            } else if (requestCode == REQUEST_FILE) {
                try {
                    val clipData: ClipData? = data!!.clipData
                    var paths = ArrayList<String>()
                    if (clipData != null) {
                        for (i in 0 until clipData.itemCount) {
                            val item = clipData.getItemAt(i)
                            val uri = item.uri
                            picturePath = getRealPathFromURI(uri)
                            paths.add(picturePath)

                        }
                    }else if(data!!.data != null ){
                        picturePath = getRealPathFromURI(data!!.data)
                        paths.add(picturePath)
                    }

                    val conMgr:ConnectivityManager =  getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork:NetworkInfo? = conMgr.getActiveNetworkInfo()
                    if (activeNetwork != null && activeNetwork.isConnected()) {
                        // network is connected
                        val uploadAsyncTask = UploadAsyncTask(this,paths,uploadStatusBar,node,reportPhotoFileAdapter,reportPhotos,filePath,requestId)
                        uploadAsyncTask.execute()
                    }else {
                        //network is not connected
                        //Toast.makeText(this, "インターネットに接続されたタイミングでアップロードを開始します。", Toast.LENGTH_SHORT).show()
                        sendBroadcastReceiver(paths,node,filePath,requestId)
                        dialogPopup(this,MESSAGE_TYPE.WARNING,"インターネットに接続されたタイミングでアップロードを開始します。")

                    }

                } catch (e: Exception) {

                    e.printStackTrace()

                    Toast.makeText(this, resources.getString(R.string.CERR_006), Toast.LENGTH_LONG).show()

                }
            }
        }else if(requestCode == REQUEST_REGIST_REPORT_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                // TODO Update your TextView.
                reportPhoto = Gson().fromJson(data!!.getStringExtra("report_photo"),ReportPhoto::class.java)
                getReportPhoto()
            }

        }
    }


    fun getRealPathFromURI(contentUri: Uri): String {
        val proj = arrayOf(MediaStore.Images.Media.DATA)

        //This method was deprecated in API level 11
        //Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        val cursorLoader = CursorLoader(
                this,
                contentUri, proj, null, null, null)
        val cursor = cursorLoader.loadInBackground()

        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }


    fun sendBroadcastReceiver(paths: ArrayList<String>,
                             node:IDragDropModel,
                             filePath:String,requestId:String) {
        var folderId = ""
        if (node!!.report_photo_file_id!! > 0) {
            folderId = node!!.report_photo_file_id.toString()
        }
        var items:MutableList<SendBroadcastReceiverModel> = mutableListOf()
        var broadcastData = PreferencesHelper.getString(applicationContext,Constant.UPLOAD_PENDING_PHOTO_DATA)
        if(broadcastData!=null && !broadcastData.equals("")) {
            items = Gson().fromJson(broadcastData, object : TypeToken<List<SendBroadcastReceiverModel>>() {}.type)
        }
        for (i in 0 until paths.count()) {
            picturePath = paths[i]
            newfile = File(picturePath)
            var path = newfile!!.name;
            if (!filePath.isNullOrEmpty()) {
                path = filePath + "/" + path;
            }
            var item = SendBroadcastReceiverModel(cookie = IJNetApiService.REQUEST_COOKIES,request_id = requestId,path = path,folder_flg = "0",folder_id = folderId,user_id =SystemInfo.user.user_id,local_file_path = picturePath)
            items.add(item)

        }
        PreferencesHelper.putString(applicationContext,Constant.UPLOAD_PENDING_PHOTO_DATA,Gson().toJson(items))

    }

}

private class UploadAsyncTask(var context: Context,var paths: ArrayList<String>,var progressBar:ProgressBar?,
             var node:IDragDropModel, var reportPhotoFileAdapter:ReportPhotoAdapter,var reportPhotos: MutableList<IDragDropModel?>? = mutableListOf(),
                 var filePath:String,var requestId:String) : AsyncTask<String, Int, String>() {
    var picturePath = ""
    var newfile: File? = null
    override fun doInBackground(vararg params: String): String {
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()

        var folderId = ""
        if (node!!.report_photo_file_id!! > 0) {
            folderId = node!!.report_photo_file_id.toString()
        }
        var incre:Int = 100/paths.count()
        var dummy = 0
        for (i in 0 until paths.count()) {
            picturePath = paths[i]
            newfile = File(picturePath)

            var path = newfile!!.name;
            if (!filePath.isNullOrEmpty()) {
                path = filePath + "/" + path;
            }
            var call: Call<ResponseBody> = repository.setReportPhotoFile(requestId, path, "0", SystemInfo.user.user_id, folderId, newfile!!)
            var body: String = call.execute().body()!!.string()
            Log.d("result:", body)
            var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
            if (result.status == IJNetApiService.ApiStatus.OK.value) {
                var item = IDragDropModel(id = 0, checked = false, file_path = result.data!!.file_path,
                        file_name = "",
                        thumbnail_file_path = result.data!!.file_path,
                        comment = "", manual = "", upload_status = 0,
                        report_photo_file_id = result.data!!.id.toLong(),
                        childrens = null, parent = null, isDirectory = false, display = "", useNum = 0, checkNum = 0)
                reportPhotos!!.add(item)
                dummy = dummy + incre
                progressBar!!.progress = dummy
                publishProgress(dummy)

            }
        }

        return "アップロードが完了しました。"
    }

    override fun onPreExecute() {
        super.onPreExecute()
        progressBar!!.progress = 0
        progressBar!!.visibility = View.VISIBLE
    }
    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        progressBar!!.visibility = View.GONE
        reportPhotoFileAdapter!!.updateDataSet(reportPhotos!!)
        Toast.makeText(context,
                result, Toast.LENGTH_LONG).show()

    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(values[0])

        reportPhotoFileAdapter!!.updateDataSet(reportPhotos!!)

    }
}







