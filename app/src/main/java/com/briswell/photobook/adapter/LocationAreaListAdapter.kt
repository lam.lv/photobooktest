package com.briswell.photobook.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.briswell.photobook.R
import android.os.Handler
import android.support.v4.content.ContextCompat
import com.briswell.photobook.activity.S1021Activity
import com.briswell.photobook.util.Constant
import com.briswell.photobook.util.ValueUtil
import com.briswell.photobook.util.api.model.LocationArea


class LocationAreaListAdapter(val activity: Activity, dataSet: MutableList<LocationArea?>?) :
RecyclerView.Adapter<LocationAreaListAdapter.ViewHolder>() {

    var dataset: MutableList<LocationArea?>? = null
        private set
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    init {
        this.dataset = dataSet
    }
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        var view: View? = null
        if (viewType == Constant.VIEW_TYPE_ITEM) {
             view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_location_area_list, parent, false) as View

        } else if (viewType == Constant.VIEW_TYPE_LOADING) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)

        }
        return ViewHolder(view!!)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if(holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            var id: TextView? = null
            var name: TextView? = null
            var address: TextView? = null
            var maintenanceFlag: TextView? = null
            var select: Button? = null


            id = holder.view.findViewById<TextView>(R.id.id)
            name = holder.view?.findViewById<TextView>(R.id.name)
            address = holder.view?.findViewById<TextView>(R.id.address)
            maintenanceFlag = holder.view?.findViewById<TextView>(R.id.maintenance_flag)
            select = holder.view?.findViewById<Button>(R.id.select)

            var item = this.dataset!![position]!!
            id?.text = item.id
            name?.text = item.name
            address?.text = ValueUtil.getPrefectures().get(item.prefecture)+ item.city?.toString().orEmpty() + item.address1?.toString().orEmpty() + item.address2?.toString().orEmpty()

            if(item.maintenanceFlag.toString().equals("1")) {
                maintenanceFlag?.text = "保守"
            }else {
                maintenanceFlag?.text = "保守外"
            }


            select!!.setOnClickListener() {
                val resultIntent = Intent(this.activity.getApplicationContext(),S1021Activity::class.java)
                resultIntent.putExtra("id", dataset!![position]!!.id)
                resultIntent.putExtra("name", dataset!![position]!!.name)
                this.activity.setResult(Activity.RESULT_OK, resultIntent)
                this.activity.finish()
            }
            if(position %2 == 1)
            {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, R.color.pink))
            }
            else
            {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }
        }

    }
    fun addLoadingView() {

        //add loading item
        Handler().post {
            dataset!!.add(null)
            notifyItemInserted(dataset!!.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        dataset!!.removeAt(dataset!!.size - 1)
        notifyItemRemoved(dataset!!.size)
        notifyItemRangeChanged(dataset!!.size,getItemCount());
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return  dataset!!.size
    }
    override fun getItemViewType(position: Int): Int {
        return if (dataset!!.get(position) == null) Constant.VIEW_TYPE_LOADING else Constant.VIEW_TYPE_ITEM
    }

}
