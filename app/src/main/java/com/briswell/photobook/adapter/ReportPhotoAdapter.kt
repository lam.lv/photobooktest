package com.briswell.photobook.adapter

/**
 * Created by luc.nt on 5/7/2018.
 */

import android.app.Activity
import android.content.ClipData
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.briswell.photobook.R
import com.briswell.photobook.helper.OnStartDragListener
import com.briswell.photobook.model.IDragDropModel
import com.briswell.photobook.helper.DragListener
import com.briswell.photobook.helper.Listener
import com.briswell.photobook.util.Constant
import com.bumptech.glide.Glide
import org.jetbrains.anko.image
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import java.util.*
import android.widget.Toast
import com.briswell.photobook.activity.S1031Activity
import android.widget.ImageView.ScaleType
import android.widget.LinearLayout
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.dialog.ImageViewActivity
import com.briswell.photobook.dialog.InputDirectoryNameDialog
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call


internal class ReportPhotoAdapter(val activity: Activity, val selfView:RecyclerView, dataset: MutableList<IDragDropModel?>, private val listener: Listener?, val dragStartListener:OnStartDragListener) : RecyclerView.Adapter<ReportPhotoAdapter.ViewHolder>() {
    private var itemSize:Int = 200
    private var mDragStartListener: OnStartDragListener?= null
    private var isImageFitToScreen = false
    private var loadingPosition = -1
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        var image:ImageView? = null
        var itemLayout: LinearLayout? = null
        var checked:CheckBox?=null
        var description:TextView?= null
        var checkNum:TextView?=null
        init {
            image = view.findViewById<ImageView>(R.id.image)
            description = view.findViewById<TextView>(R.id.description)
            itemLayout = view.findViewById<LinearLayout>(R.id.item_layout)
            checked = view.findViewById(R.id.checked)
            checkNum = view.findViewById<TextView>(R.id.check_num)
        }
    }



    var dataset: MutableList<IDragDropModel?>? = null
        private set



    val dragInstance: DragListener?
        get() {
            if (listener != null) {
                return DragListener(this.activity,selfView)
            } else {
                Log.e("ListAdapter", "Listener wasn't initialized!")
                return null
            }
        }

    init {
        this.dataset = dataset
        mDragStartListener = dragStartListener
    }

    fun addLoadingView() {
        //add loading item
        Handler().post {
            dataset!!.add(null)
            loadingPosition = dataset!!.size - 1
            notifyItemInserted(dataset!!.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        dataset!!.removeAt(loadingPosition)
        notifyItemRemoved(dataset!!.size)
        notifyItemRangeChanged(0,getItemCount());
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view:View? = null

        if (viewType == Constant.VIEW_TYPE_ITEM) {
            view = LayoutInflater.from(
                    parent.context).inflate(R.layout.item_photo, parent, false)



        } else if (viewType == Constant.VIEW_TYPE_LOADING) {
             view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)

        }

        return ViewHolder(view!!)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            if(!dataset!![position]!!.isDirectory!!)  {
                Glide.with(activity).load(dataset!![position]!!.file_path).into(holder.image)
                val params = holder.itemLayout!!.getLayoutParams()
                params.width = itemSize
                params.height = itemSize
                holder.itemLayout!!.setLayoutParams(params)
                holder.checked!!.visibility = View.VISIBLE
                holder.checkNum!!.visibility = View.VISIBLE

                holder.itemLayout!!.tag = position
                holder.checked!!.isChecked = dataset!![position]!!.checked!!
                holder.checkNum!!.setText(dataset!![position]!!.checkNum.toString())
                holder.checked!!.setOnClickListener(View.OnClickListener { v ->
                    dataset!![position]!!.checked = (v as CheckBox).isChecked
                    if((v as CheckBox).isChecked) {
                        var max = 0
                        for(item in dataset!!) {
                            if(item!!.checkNum>max) {
                                max = item!!.checkNum
                            }
                        }
                        max = max+1
                        dataset!![position]!!.checkNum = max

                    }else {
                        for(item in dataset!!) {
                            if(item!!.checkNum> dataset!![position]!!.checkNum) {
                                item!!.checkNum = item.checkNum -1
                            }
                        }
                        dataset!![position]!!.checkNum = 0
                    }
                    updateDataSet(dataset!!)
                    holder.checkNum!!.setText(dataset!![position]!!.checkNum.toString())

                })
                holder.description!!.setText(dataset!![position]!!.useNum.toString()+"回")
                holder.itemView.setOnClickListener {
                    //v -> Toast.makeText(v.context, position.toString() + " is a image", Toast.LENGTH_SHORT).show()

                }
               holder.view!!.setOnClickListener(){
                   val intent = Intent(this.activity, ImageViewActivity::class.java)
                   intent.putExtra("url", dataset!![position]!!.file_path)
                   activity.startActivity(intent)
                   return@setOnClickListener
               }
                holder.view.setOnLongClickListener(View.OnLongClickListener { v ->
                    val data = ClipData.newPlainText("", "")
                    val shadowBuilder = View.DragShadowBuilder(v)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        v.startDragAndDrop(data, shadowBuilder, v, 0)
                    } else {
                        v.startDrag(data, shadowBuilder, v, 0)
                    }
                    true
                })
            }else {
                val params = holder.itemLayout!!.getLayoutParams()
                params.width = itemSize
                params.height = itemSize
                holder.itemLayout!!.setLayoutParams(params)
                holder.image!!.setImageResource(R.drawable.folder_icon)
                holder.description!!.setText(dataset!![position]!!.file_name)
                holder.image!!.adjustViewBounds = true
                holder.checked!!.visibility = View.INVISIBLE
                holder.checkNum!!.visibility = View.INVISIBLE
                holder.itemView.setOnClickListener {
                    //v -> Toast.makeText(v.context, position.toString() + " is a dir", Toast.LENGTH_SHORT).show()
                    var node:IDragDropModel = IDragDropModel(checked = false,id = null,file_path = "",
                            file_name = "",thumbnail_file_path = "",report_photo_file_id =dataset!![position]!!.report_photo_file_id ,
                            isDirectory = false,display = "",comment = "",manual = "",
                            upload_status = 0,parent = (activity as S1031Activity).node,childrens = null,useNum = 0,checkNum = 0)
                    (activity as S1031Activity).node = node
                    activity.getReportPhotoFiles()
                }
                holder.view.setOnLongClickListener(View.OnLongClickListener { v ->
                    Log.d("IJNET","rename folder")
                    var dialog: InputDirectoryNameDialog = InputDirectoryNameDialog(activity, object : InputDirectoryNameDialog.InputDirectoryNameDialogListener {
                        override fun onOK(name: String) {
                            val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
                            var call: Call<ResponseBody> = repository.updateReportPhotoFile(dataset!![position]!!.report_photo_file_id.toString(), name,  SystemInfo.user.user_id)
                            var body: String = call.execute().body()!!.string()
                            var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
                            if (result.status == IJNetApiService.ApiStatus.OK.value) {
                                dataset!![position]!!.file_name = name
                                notifyItemChanged(position)
                            }

                        }

                        override fun onCancel() {

                        }
                    },true)
                    dialog.show()
                     true
                })
            }

        }
    }


    override fun getItemCount(): Int {
        return dataset!!.size
    }

    fun updateDataSet(list: MutableList<IDragDropModel?>) {
        this.dataset = list
        this.notifyDataSetChanged()
    }

     override fun getItemViewType(position: Int): Int {
        return if (dataset!!.get(position) == null) Constant.VIEW_TYPE_LOADING else Constant.VIEW_TYPE_ITEM
    }

    fun getItemSize():Int {
        return itemSize
    }
    fun setItemSize(size:Int){
        itemSize = size;
        this.notifyDataSetChanged()
    }

}
