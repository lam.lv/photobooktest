package com.briswell.photobook.adapter

/**
 * Created by luc.nt on 5/7/2018.
 */

import android.app.Activity
import android.content.ClipData
import android.content.DialogInterface
import android.os.Build
import android.os.Handler
import android.support.v4.view.MotionEventCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import com.briswell.photobook.R
import com.briswell.photobook.model.IDragDropModel
import com.bumptech.glide.Glide
import java.util.*
import android.text.Editable
import android.text.TextWatcher
import android.widget.*
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.activity.S1031Activity
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.helper.*
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.briswell.photobook.util.api.repository.RequestRepositoryProvider
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ResponseBody
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import org.jetbrains.anko.sdk25.coroutines.onClick
import retrofit2.Call
import android.view.MotionEvent
import com.briswell.photobook.util.Constant


internal class ReportPhotoDetailAdapter(val activity: Activity, val selfView:RecyclerView,  dataset: MutableList<IDragDropModel?>, private val listener: Listener?, val dragStartListener:OnStartDragListener ) : RecyclerView.Adapter<ReportPhotoDetailAdapter.ViewHolder>(){

    public var mDragStartListener: OnStartDragListener?= null
    private var loadingPosition = -1
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        var display:TextView? = null
        var image:ImageView? = null
        var itemLayout:LinearLayout? = null
        var comment1:EditText?= null
        var comment2:EditText?= null
        var check:CheckBox?= null
        var clearImage:Button?= null

        init {
            itemLayout = view.findViewById<LinearLayout>(R.id.item_layout)
            display = view.findViewById(R.id.display)
            image = view.findViewById<ImageView>(R.id.image)
            comment1 = view.findViewById<EditText>(R.id.comment1)
            comment2 = view.findViewById<EditText>(R.id.comment2)
            check = view.findViewById(R.id.check_image)
            clearImage = view.findViewById(R.id.clear_image)
        }

    }

    var dataset: MutableList<IDragDropModel?>? = null

    val dragInstance: DragListener?
        get() {
            if (listener != null) {
                return DragListener(this.activity,selfView)
            } else {
                Log.e("ListAdapter", "Listener wasn't initialized!")
                return null
            }
        }

    init {
        this.dataset = dataset
        mDragStartListener = dragStartListener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view:View? = null
        if (viewType == Constant.VIEW_TYPE_ITEM) {
            view = LayoutInflater.from(
                    parent.context).inflate(R.layout.item_photo_detail, parent, false)
            view.setOnLongClickListener(View.OnLongClickListener { v ->
                val data = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(v)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, shadowBuilder, v, 0)
                } else {
                    v.startDrag(data, shadowBuilder, v, 0)
                }
                true
            })
        }else if (viewType == Constant.VIEW_TYPE_LOADING) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)

        }else if (viewType == Constant.VIEW_TYPE_DISABLE) {
            view = LayoutInflater.from(
                    parent.context).inflate(R.layout.item_photo_detail, parent, false)
        }


        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            holder.display?.setText(dataset!![position]!!.display)
            if (!dataset!![position]!!.file_path.isNullOrEmpty()) {
                Glide.with(activity).load(dataset!![position]!!.file_path).into(holder.image)
            } else {
                holder.image!!.setImageResource(R.drawable.no_image)
            }
            holder.comment1?.setText(dataset!![position]!!.comment)
            holder.comment2?.setText(dataset!![position]!!.manual)
            holder.itemLayout!!.tag = position!!
            holder.check!!.isChecked = dataset!![position]!!.checked!!
            holder.display!!.setText(getDisplaySeq(position))
            holder.comment1!!!!.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {

                }

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int,
                                           before: Int, count: Int) {
                    try {
                        dataset!![holder.adapterPosition]!!.comment = s.toString();
                    } catch (e: Exception) {
                    }

                }
            })
            holder.comment2!!.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int,
                                           before: Int, count: Int) {
                    try {
                        dataset!![holder.adapterPosition]!!.manual = s.toString()
                    } catch (e: Exception) {
                    }
                }
            })
            holder.check!!.setOnCheckedChangeListener { buttonView, isChecked ->
                try {
                    dataset!![holder.adapterPosition]!!.checked = isChecked
                    holder.check!!.isChecked = dataset!![holder.adapterPosition]!!.checked!!
                } catch (e: Exception) {
                }
            }

            holder.clearImage!!.setOnClickListener() {
                (this.activity as S1031Activity).showMessageOKCancel(this.activity.getResources().getString(R.string.CFRM_002),
                        DialogInterface.OnClickListener { dialog, which ->
                            dataset!![position]!!.report_photo_file_id = 0
                            dataset!![position]!!.file_path = ""
                            notifyItemChanged(position)
                        })

            }
        } else if(holder.itemViewType == Constant.VIEW_TYPE_DISABLE) {
            holder.display?.setText(dataset!![position]!!.display)
            if (!dataset!![position]!!.file_path.isNullOrEmpty()) {
                Glide.with(activity).load(dataset!![position]!!.file_path).into(holder.image)
            } else {
                holder.image!!.setImageResource(R.drawable.no_image)
            }
            holder.comment1?.setText(dataset!![position]!!.comment)
            holder.comment2?.setText(dataset!![position]!!.manual)
            holder.itemLayout!!.tag = position!!
            holder.check!!.isChecked = dataset!![position]!!.checked!!
            holder.check!!.isEnabled = false
            holder.comment1!!.isEnabled = false
            holder.comment2!!.isEnabled = false

        }
    }
    fun addLoadingView() {
        //add loading item
        Handler().post {
            dataset!!.add(null)
            loadingPosition = dataset!!.size - 1
            notifyItemInserted(dataset!!.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        dataset!!.removeAt(loadingPosition)
        notifyItemRemoved(dataset!!.size)
        notifyItemRangeChanged(0,getItemCount());
    }
    fun getDisplaySeq(position:Int):String {
        var templateType:Int  = (activity as S1031Activity).reportPhoto!!.template_type.toInt()
        var display = ""
        if(position>0) {
            display = ((position-1)/templateType +1).toString() + "-"
            if((position)%templateType != 0) {
                display = display + ((position)%templateType).toString()
            }else {
                display = display + templateType.toString()
            }
        }
        return display
    }
    override fun getItemCount(): Int {
        return dataset!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataset!!.get(position) == null) Constant.VIEW_TYPE_LOADING
        else if(dataset!!.get(position)!!.isLabel) Constant.VIEW_TYPE_DISABLE
        else Constant.VIEW_TYPE_ITEM
    }



}
