package com.briswell.photobook.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.briswell.photobook.R
import com.briswell.photobook.util.api.model.Request
import android.os.Handler
import android.support.v4.content.ContextCompat
import com.briswell.photobook.activity.S1031Activity
import com.briswell.photobook.util.Constant
import com.briswell.photobook.util.DateUtil
import java.text.SimpleDateFormat
import java.util.*


class RequestListAdapter(val activity: Activity, dataSet: MutableList<Request?>?) :
RecyclerView.Adapter<RequestListAdapter.ViewHolder>() {

    var dataset: MutableList<Request?>? = null
        private set
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    init {
        this.dataset = dataSet
    }
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        var view: View? = null
        if (viewType == Constant.VIEW_TYPE_ITEM) {
             view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_request_list, parent, false) as View

        } else if (viewType == Constant.VIEW_TYPE_LOADING) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)

        }
        return ViewHolder(view!!)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if(holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            var locationAreaName: TextView? = null
            var requestCode: TextView? = null
            var addFlag: TextView? = null
            var customerName: TextView? = null
            var status: TextView? = null
            var workingDate:TextView? = null
            var repairName:TextView? = null
            var deviceModel:TextView? = null
            var deviceNo:TextView? = null
            var sectionName:TextView? = null
            var staffName:TextView? = null
            var supplierName:TextView? = null
            var supplierStaffName:TextView? = null
            var situation:TextView? = null
            var messageTts:TextView? = null
            var contact1Name:TextView? = null
            var contact1StaffName:TextView? = null
            var contact1Tel1:TextView? = null
            var contact1Email:TextView? = null
            var album: Button? = null
            locationAreaName = holder.view.findViewById<TextView>(R.id.location_area_name)
            requestCode = holder.view?.findViewById<TextView>(R.id.request_code)
            addFlag = holder.view?.findViewById<TextView>(R.id.add_flag)
            customerName = holder.view?.findViewById<TextView>(R.id.customer_name)
            status = holder.view?.findViewById<TextView>(R.id.status)
            workingDate = holder.view?.findViewById<TextView>(R.id.working_date)
            repairName = holder.view?.findViewById<TextView>(R.id.repair_name)
            deviceModel = holder.view?.findViewById<TextView>(R.id.device_model)
            deviceNo = holder.view?.findViewById<TextView>(R.id.device_no)
            sectionName = holder.view?.findViewById<TextView>(R.id.section_name)
            staffName = holder.view?.findViewById<TextView>(R.id.staff_name)
            supplierName = holder.view?.findViewById<TextView>(R.id.supplier_name)
            supplierStaffName = holder.view?.findViewById<TextView>(R.id.supplier_staff_name)
            situation = holder.view?.findViewById<TextView>(R.id.situation)
            messageTts = holder.view?.findViewById<TextView>(R.id.message_tts)
            contact1Name = holder.view?.findViewById<TextView>(R.id.contact1_name)
            contact1StaffName = holder.view?.findViewById<TextView>(R.id.contact1_staff_name)
            contact1Tel1 = holder.view?.findViewById<TextView>(R.id.contact1_tel1)
            contact1Email = holder.view?.findViewById<TextView>(R.id.contact1_email)

            album = holder.view?.findViewById<Button>(R.id.album)

            var item = this.dataset!![position]!!
            locationAreaName?.text = item.locatin_area_name
            requestCode?.text = item.request_code
            if(item.add_flag != null && item.add_flag!!.toBoolean() == true) {
                addFlag?.text = "追加"
            }else {
                addFlag?.text = ""
            }
            customerName?.text = item.customer_name

            if(item.report_photo_status.trim().equals("2")) {
                status?.text = "●"
            }else if(item.report_photo_status.trim().equals("3")) {
                status?.text = "済"
            }else {
                status?.text = ""
            }

            var dateFormat = SimpleDateFormat("yyyy-MM-dd")
            var date = dateFormat.parse(item.working_date)
            var workDate = item.working_date!!.replace("-","/")
            var workingTime:String = ""
            if (item.working_start_time != "" || item.working_end_time != "") {
                workingTime = item.working_start_time + "～" + item.working_end_time
            } else {
                workingTime = ""
            }
            workDate = workDate + "("+ DateUtil.getShortDayOfWeek(date)+")"

            workingDate?.text = workDate +" " + workingTime
            repairName?.text = item.repair_name
            deviceModel?.text = item.device_model
            deviceNo?.text = item.device_no
            sectionName?.text = item.section_name
            staffName?.text = item.staff_name
            supplierName?.text = item.supplier_name
            supplierStaffName?.text = item.supplier_staff_name
            situation?.text = item.situation
            messageTts?.text = item.message_tts
            contact1Name?.text = item.contact1_name
            contact1StaffName?.text = item.contact1_staff_name
            contact1Tel1?.text = item.contact1_tel1
            contact1Email?.text = item.contact1_email

            album!!.setOnClickListener() {
                val intent = Intent(this.activity, S1031Activity::class.java)

                intent.putExtra("request_id", item.id)


                activity.startActivity(intent)
                return@setOnClickListener
            }

            if(position %2 == 1)
            {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, R.color.pink))
            }
            else
            {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }
        }

    }

    fun addLoadingView() {

        //add loading item
        Handler().post {
            dataset!!.add(null)
            notifyItemInserted(dataset!!.size - 1)
        }
    }

    fun removeLoadingView() {
        if (dataset !== null && dataset!!.size > 1) {
            dataset!!.removeAt(dataset!!.size - 1)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return  dataset!!.size
    }
    override fun getItemViewType(position: Int): Int {
        return if (dataset!!.get(position) == null) Constant.VIEW_TYPE_LOADING else Constant.VIEW_TYPE_ITEM
    }

}
