package com.briswell.photobook.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.briswell.photobook.R
import android.os.Handler
import android.support.v4.content.ContextCompat
import com.briswell.photobook.activity.S1021Activity
import com.briswell.photobook.util.Constant
import com.briswell.photobook.util.api.model.Section


class SectionListAdapter(val activity: Activity, dataSet: MutableList<Section?>?) :
RecyclerView.Adapter<SectionListAdapter.ViewHolder>() {

    var dataset: MutableList<Section?>? = null
        private set
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    init {
        this.dataset = dataSet
    }
    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        var view: View? = null
        if (viewType == Constant.VIEW_TYPE_ITEM) {
             view = LayoutInflater.from(activity)
                    .inflate(R.layout.item_section_list, parent, false) as View

        } else if (viewType == Constant.VIEW_TYPE_LOADING) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)

        }
        return ViewHolder(view!!)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if(holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            var sectionCode: TextView? = null
            var sectionFullName: TextView? = null
            var sectionName: TextView? = null
            var hierarchy: TextView? = null
            var currntFlag: TextView? = null
            var select: Button? = null


            sectionCode = holder.view.findViewById<TextView>(R.id.section_code)
            sectionFullName = holder.view?.findViewById<TextView>(R.id.section_full_name)
            sectionName = holder.view?.findViewById<TextView>(R.id.section_name)
            hierarchy = holder.view?.findViewById<TextView>(R.id.hierarchy)
            currntFlag = holder.view?.findViewById<TextView>(R.id.currnt_flag)
            select = holder.view?.findViewById<Button>(R.id.select)

            var item = this.dataset!![position]!!
            sectionCode?.text = item.sectionCode
            sectionFullName?.text = item.sectionFullName
            sectionName?.text = item.sectionName
            when (item.hierarchy){
                "0" -> {
                    hierarchy?.text = "全社"
                }
                "1" -> {
                    hierarchy?.text = "部門"
                }
                "2" -> {
                    hierarchy?.text = "課"
                }
                "3" -> {
                    hierarchy?.text = "グループ"
                }
            }
            when (item.currentFlag) {
                "1" -> {
                    currntFlag?.text = "稼働"
                }
                "0" -> {
                    currntFlag?.text = "非稼働"
                }
            }


            select!!.setOnClickListener() {
                val resultIntent = Intent(this.activity.getApplicationContext(),S1021Activity::class.java)
                resultIntent.putExtra("id", dataset!![position]!!.id)
                resultIntent.putExtra("sectionName", dataset!![position]!!.sectionName)
                this.activity.setResult(Activity.RESULT_OK, resultIntent)
                this.activity.finish()
            }
            if(position %2 == 1)
            {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, R.color.pink))
            }
            else
            {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }
        }

    }
    fun addLoadingView() {

        //add loading item
        Handler().post {
            dataset!!.add(null)
            notifyItemInserted(dataset!!.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        dataset!!.removeAt(dataset!!.size - 1)
        notifyItemRemoved(dataset!!.size)
        notifyItemRangeChanged(dataset!!.size,getItemCount());
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return  dataset!!.size
    }
    override fun getItemViewType(position: Int): Int {
        return if (dataset!!.get(position) == null) Constant.VIEW_TYPE_LOADING else Constant.VIEW_TYPE_ITEM
    }

}
