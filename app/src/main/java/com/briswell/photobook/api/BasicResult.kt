package com.briswell.photobook.api

/**
 * Created by luc.nt on 4/19/2018.
 */
data class BasicResult (
        val status: Long,
        val data: Any?,
        val message:String
)