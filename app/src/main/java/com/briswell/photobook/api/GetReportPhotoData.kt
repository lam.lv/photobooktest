package com.briswell.photobook.api

import com.briswell.photobook.api.model.ReportPhoto
import com.briswell.photobook.api.model.ReportPhotoDetail
import com.briswell.photobook.api.model.ReportPhotoFile
import com.briswell.photobook.util.api.model.Request

/**
 * Entire search result data class
 */
data class GetReportPhotoData (
        val request_data: Request,
        val report_photo_data: ReportPhoto?,
        val report_photo_file_data:List<ReportPhotoFile>?,
        val report_photo_detail_data:List<ReportPhotoDetail>?

)