package com.briswell.photobook.api

import com.briswell.photobook.util.api.model.LocationArea
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.model.Supplier

/**
 * Entire search result data class
 */
data class GetReportPhotoResult (
        val status:Long,
        val message:String,
        val mode:String,
        val data:GetReportPhotoData

)