package com.briswell.photobook.api

import com.briswell.photobook.util.api.model.Request

/**
 * Entire search result data class
 */
data class GetRequestResult (
        val status:Long,
        val message:String,
        val tts_order_count:String,
        val request_count:String,
        val data: List<Request>?

)