package com.briswell.photobook.api

import com.briswell.photobook.util.api.model.Section

/**
 * Entire search result data class
 */
data class GetSectionResult (
        val status:Long,
        val message:String,
        val count:String,
        val data: List<Section>?

)