package com.briswell.photobook.api

import com.briswell.photobook.util.api.model.Staff

/**
 * Entire search result data class
 */
data class GetStaffResult (
        val status:Long,
        val message:String,
        val count:String,
        val data: List<Staff>?

)