package com.briswell.photobook.api

import com.briswell.photobook.api.model.Template
import com.briswell.photobook.api.model.TemplateDetail
import com.briswell.photobook.util.api.model.LocationArea
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.model.Supplier

/**
 * Entire search result data class
 */
data class GetTemplateDetailResult (
        val status:Long,
        val message:String,
        val data:List<TemplateDetail>?

)