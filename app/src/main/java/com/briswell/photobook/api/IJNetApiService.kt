package com.briswell.photobook.api


import com.briswell.photobook.BuildConfig
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


interface IJNetApiService {

    @retrofit2.http.GET("get_request")
    fun getRequest(@Header("Cookie") cookie: String,@Query("apikey") apikey:String, @Query("passphrase") passphrase:String,
                   @Query("version") version:String, @Query("working_date_from") working_date_from:String,
                   @Query("working_date_to") working_date_to:String, @Query("location_area_name") location_area_name:String,
                   @Query("location_area_id") location_area_id:String, @Query("tts_order_id") tts_order_id:String,
                   @Query("section_id") section_id:String, @Query("staff_id") staff_id:String,
                   @Query("supplier_id") supplier_id:String, @Query("page") page:String
    ): Call<ResponseBody>


    @Multipart
    @retrofit2.http.POST("set_report_photo_file")
    fun setReportPhotoFile(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                           @Query("passphrase") passphrase:String,@Query("version") version:String,
                           @Part("request_id") request_id: RequestBody,
                           @Part("file_path") file_path: RequestBody,
                           @Part("folder_flag") folder_flag: RequestBody,
                           @Part("user_id") user_id: RequestBody,
                           @Part("folder_id") folder_id: RequestBody,
                           @Part file: MultipartBody.Part?


    ): Call<ResponseBody>

    @Multipart
    @retrofit2.http.POST("update_report_photo_file")
    fun updateReportPhotoFile(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                           @Query("passphrase") passphrase:String,@Query("version") version:String,
                           @Part("id") id: RequestBody,
                           @Part("file_path") file_path: RequestBody,
                           @Part("user_id") user_id: RequestBody


    ): Call<ResponseBody>

    @Multipart
    @retrofit2.http.POST("set_report_photo_detail")
    fun setReportPhotoDetail(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                           @Query("passphrase") passphrase:String,@Query("version") version:String,
                           @Part("mode") mode: RequestBody,
                           @Part("id") id: RequestBody,
                           @Part("report_photo_id") report_photo_id: RequestBody,
                           @Part("report_photo_file_id") report_photo_file_id: RequestBody,
                           @Part("comment") comment: RequestBody,
                           @Part("manual") manual: RequestBody,
                           @Part("sort") sort: RequestBody,
                           @Part("user_id") user_id: RequestBody
    ): Call<ResponseBody>


    @Multipart
    @retrofit2.http.POST("set_report_photo")
    fun setReportPhoto(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                             @Query("passphrase") passphrase:String,@Query("version") version:String,
                             @Part("mode") mode: RequestBody,
                             @Part("id") id: RequestBody,
                             @Part("location_area_id") location_area_id: RequestBody,
                             @Part("tts_order_id") tts_order_id: RequestBody,
                             @Part("request_id") request_id: RequestBody,
                             @Part("template_type") template_type: RequestBody,
                             @Part("customer_name") customer_name: RequestBody,
                             @Part("title") title: RequestBody,
                             @Part("location_name") location_name: RequestBody,
                             @Part("working_date") working_date: RequestBody,
                             @Part("invoice_name_suffix") invoice_name_suffix: RequestBody,
                             @Part("user_id") user_id: RequestBody

    ): Call<ResponseBody>

    @FormUrlEncoded
    @retrofit2.http.POST("login")
    fun login(@Query("apikey") apikey:String,@Query("passphrase") passphrase:String , @Query("version") version:String ,@FieldMap  parameters: Map<String, String>): Call<ResponseBody> //io.reactivex.Observable<LoginResult>


    @retrofit2.http.GET("get_section")
    fun getSection(@Header("Cookie") cookie: String,@Query("apikey") apikey:String, @Query("passphrase") passphrase:String,
                   @Query("version") version:String, @Query("hierarchy") hierarchy:String,
                   @Query("current_flag") current_flag:String,@Query("page") page:String
                   ): Call<ResponseBody>

    @retrofit2.http.GET("get_supplier")
    fun getSupplier(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                    @Query("passphrase") passphrase:String,
                    @Query("version") version:String,
                    @Query("kana") kana:String,
                    @Query("name") name:String,
                    @Query("integration_flag") integration_flag:String,
                    @Query("overtime_work") overtime_work:String,
                    @Query("prefecture") prefecture:String,
                    @Query("city") city:String,
                    @Query("supplier_tts") supplier_tts:String,
                    @Query("page") page:String,
                    @Query("count") count:String
    ): Call<ResponseBody>

    @retrofit2.http.GET("get_location_area")
    fun getLocationArea(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                    @Query("passphrase") passphrase:String,
                    @Query("version") version:String,
                    @Query("kana") kana:String?,
                    @Query("name") name:String?,
                    @Query("maintenance_flag") maintenance_flag:String?,
                    @Query("prefecture") prefecture:String?,
                    @Query("city") city:String?,
                    @Query("address1") address1:String?,
                    @Query("page") page:String,
                    @Query("count") count:String
    ): Call<ResponseBody>

    @retrofit2.http.GET("get_staff")
    fun getStaff(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                    @Query("passphrase") passphrase:String,
                    @Query("version") version:String?,
                    @Query("staff_name") staff_name:String?,
                    @Query("section_id") section_id:String?,
                    @Query("current_flag") current_flag:String?,
                    @Query("approve_authority") approve_authority:String?
    ): Call<ResponseBody>

    @retrofit2.http.GET("get_report_photo_file")
    fun getReportPhotoFile(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                            @Query("passphrase") passphrase:String,
                            @Query("version") version:String,
                            @Query("request_id") request_id:String,
                            @Query("report_photo_file_id") report_photo_file_id:String?,
                            @Query("file_path") file_path:String?

    ): Call<ResponseBody>

    @retrofit2.http.GET("get_report_photo")
    fun getReportPhoto(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                           @Query("passphrase") passphrase:String,
                           @Query("version") version:String,
                           @Query("request_id") request_id:String

    ): Call<ResponseBody>


    @retrofit2.http.GET("get_template")
    fun getTemplate(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                       @Query("passphrase") passphrase:String,
                       @Query("version") version:String
    ): Call<ResponseBody>

    @retrofit2.http.GET("get_template_detail")
    fun getTemplateDetail(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                    @Query("passphrase") passphrase:String,
                    @Query("version") version:String,
                    @Query("id") id:String
    ): Call<ResponseBody>

    @Streaming
    @retrofit2.http.GET("download_report_photo")
    fun downloadReportPhoto(@Header("Cookie") cookie: String,@Query("apikey") apikey:String,
                          @Query("passphrase") passphrase:String,
                          @Query("version") version:String,
                          @Query("request_id") request_id:String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @retrofit2.http.POST("updatephotodetail")
    fun updatephotodetail(@FieldMap  parameters: Map<String, String>): io.reactivex.Observable<BasicResult>


    @FormUrlEncoded
    @retrofit2.http.POST("removePhotoDetail")
    fun removePhotoDetail(@FieldMap  parameters: Map<String, String>): io.reactivex.Observable<BasicResult>

    @FormUrlEncoded
    @retrofit2.http.POST("photodetaillist")
    fun photoDetailLists(@FieldMap  parameters: Map<String, String>): io.reactivex.Observable<ReportPhotoDetailResult>

    /**
     * Companion object for the factory
     */
    companion object Factory {

        val key = "kc0hVdEDMbouY3QU"
        var REQUEST_COOKIES = ""
        fun create(): IJNetApiService {
            val client = OkHttpClient.Builder()
                    .readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .build()
            val retrofit = retrofit2.Retrofit.Builder()
                    .baseUrl(BuildConfig.apiUrl)
                    .client(client)
                    .build()

            return retrofit.create(IJNetApiService::class.java);
        }

    }
    public enum class ApiStatus(val value:Long) {
        OK(200),BAD(400),ERROR(500)
    }
}