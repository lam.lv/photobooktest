package com.briswell.photobook.api

import java.math.BigInteger

/**
 * Entire search result data class
 */
data class LoginResult (
        val status: Long,
        val data: Any?,
        val message:String,
        val user_id:String?,
        val user_flg:Int,
        val staff_id:BigInteger?,
        val suplier_staff_id:BigInteger?

)