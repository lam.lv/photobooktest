package com.briswell.photobook.api

/**
 * Entire search result data class
 */
data class RegistUserResult (
        val status: Long,
        val data: Any?,
        val message:String
)