package com.briswell.photobook.api

import com.briswell.photobook.api.model.ReportPhotoDetail

/**
 * Entire search result data class
 */
data class ReportPhotoDetailItem (
        val total:Long,
        val photodetaillist: List<ReportPhotoDetail>?
)