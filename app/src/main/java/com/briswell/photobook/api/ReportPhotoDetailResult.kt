package com.briswell.photobook.api

/**
 * Entire search result data class
 */
data class ReportPhotoDetailResult (
        val status:Long,
        val data: ReportPhotoDetailItem?,
        val message:String
)