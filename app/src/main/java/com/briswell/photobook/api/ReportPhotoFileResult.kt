package com.briswell.photobook.api

import com.briswell.photobook.api.model.ReportPhotoFile

/**
 * Entire search result data class
 */
data class ReportPhotoFileResult (
        val status:Long,
        val data: List<ReportPhotoFile>?,
        val message:String
)