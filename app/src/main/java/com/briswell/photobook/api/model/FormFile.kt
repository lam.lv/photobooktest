package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 4/16/2018.
 */
data class FormFile(
        @SerializedName("name") val name: String,
        @SerializedName("contentType") val contentType: String,
        @SerializedName("filePath") val filePath: String

        )