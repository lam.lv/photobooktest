package com.briswell.photobook.util.api.model

import android.os.Parcel
import android.os.Parcelable
import com.briswell.photobook.api.model.FormFile
import com.google.gson.annotations.SerializedName
import java.io.File

/**
 * Created by luc.nt on 4/2/2018.
 */
data class LocationArea(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("prefecture") val prefecture: String?,
        @SerializedName("city") val city: String?,
        @SerializedName("address1") val address1: String?,
        @SerializedName("address2") val address2: String?,
        @SerializedName("maintenance_flag") val maintenanceFlag: String?
)