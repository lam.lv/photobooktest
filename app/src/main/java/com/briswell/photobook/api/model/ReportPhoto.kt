package com.briswell.photobook.api.model

import com.briswell.photobook.util.api.model.LocationArea
import com.briswell.photobook.util.api.model.Section
import com.briswell.photobook.util.api.model.Supplier
import com.google.gson.annotations.SerializedName

/**
 * Entire search result data class
 */
data class ReportPhoto (
        @SerializedName("id") var id:String,
        @SerializedName("tts_order_id") var tts_order_id:String,
        @SerializedName("request_id") var request_id:String,
        @SerializedName("template_type") var template_type:String,
        @SerializedName("customer_name") var customer_name:String,
        @SerializedName("title") var title:String,
        @SerializedName("location_area_id") var location_area_id:String,
        @SerializedName("location_name") var location_name:String,
        @SerializedName("working_date") var working_date:String,
        @SerializedName("invoice_name_suf") var invoice_name_suf:String,
        @SerializedName("count") var count:String
)