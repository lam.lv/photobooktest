package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 3/20/2018.
 */
data class ReportPhotoDetail (
        @SerializedName("id") var id:Long?,
        @SerializedName("report_photo_file_id") var report_photo_file_id:String?,
        @SerializedName("comment") var comment:String,
        @SerializedName("manual") var manual:String,
        @SerializedName("sort") var sort:Int,
        @SerializedName("file_path") var file_path:String,
        @SerializedName("thumbnail_file_path") var thumbnail_file_path:String

)