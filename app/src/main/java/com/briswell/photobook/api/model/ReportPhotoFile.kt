package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 3/20/2018.
 */
data class ReportPhotoFile (
        @SerializedName("id") val id:Int,
        @SerializedName("file_path") val file_path:String,
        @SerializedName("folder_flag") val folder_flag:String?
)