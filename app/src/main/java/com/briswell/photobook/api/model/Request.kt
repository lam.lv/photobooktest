package com.briswell.photobook.util.api.model

import android.os.Parcel
import android.os.Parcelable
import com.briswell.photobook.api.model.FormFile
import com.google.gson.annotations.SerializedName
import java.io.File

/**
 * Created by luc.nt on 4/2/2018.
 */
data class Request(
        @SerializedName("id") val id: Long,
        @SerializedName("location_area_id") val location_area_id: Long?,
        @SerializedName("locatin_area_name") val locatin_area_name: String?,
        @SerializedName("request_code") val request_code: String?,
        @SerializedName("add_flag") val add_flag: String?,
        @SerializedName("customer_name") val customer_name: String?,
        @SerializedName("report_photo_status") val report_photo_status: String,
        @SerializedName("working_date") val working_date: String?,
        @SerializedName("working_start_time") val working_start_time: String?,
        @SerializedName("working_end_time") val working_end_time: String?,
        @SerializedName("repair_name") val repair_name: String?,
        @SerializedName("device_model") val device_model: String?,
        @SerializedName("device_no") val device_no: String?,
        @SerializedName("section_name") val section_name: String?,
        @SerializedName("staff_name") val staff_name: String?,
        @SerializedName("supplier_name") val supplier_name: String?,
        @SerializedName("supplier_staff_name") val supplier_staff_name: String?,
        @SerializedName("situation") val situation: String?,
        @SerializedName("message_tts") val message_tts: String?,
        @SerializedName("contact1_name") val contact1_name: String?,
        @SerializedName("contact1_staff_name") val contact1_staff_name: String?,
        @SerializedName("contact1_tel1") val contact1_tel1: String?,
        @SerializedName("contact1_email") val contact1_email: String?,
        @SerializedName("tts_order_id") val tts_order_id: String?

        )