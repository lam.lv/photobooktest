package com.briswell.photobook.util.api.model

import android.os.Parcel
import android.os.Parcelable
import com.briswell.photobook.api.model.FormFile
import com.google.gson.annotations.SerializedName
import java.io.File

/**
 * Created by luc.nt on 4/2/2018.
 */
data class Section(
        @SerializedName("id") val id: String,
        @SerializedName("section_code") val sectionCode: String,
        @SerializedName("section_full_name") val sectionFullName: String,
        @SerializedName("section_name") val sectionName: String?,
        @SerializedName("hierarchy") val hierarchy: String?,
        @SerializedName("current_flag") val currentFlag: String?
        )