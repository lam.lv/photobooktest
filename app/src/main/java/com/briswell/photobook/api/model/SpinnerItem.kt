package com.briswell.photobook.api.model

/**
 * Created by luc.nt on 9/18/2018.
 */
 data class SpinnerItem(val value:String, val text:String) {


    //to display object as a string in spinner
    override fun toString(): String = text

    override fun equals(obj: Any?): Boolean {
        if (obj is SpinnerItem) {
            val c = obj as SpinnerItem?
            if (c!!.text.equals(text) && c!!.value.equals(value)) return true
        }

        return false
    }
}


