package com.briswell.photobook.util.api.model

import android.os.Parcel
import android.os.Parcelable
import com.briswell.photobook.api.model.FormFile
import com.google.gson.annotations.SerializedName
import java.io.File

/**
 * Created by luc.nt on 4/2/2018.
 */
data class Staff(
        @SerializedName("id") val staffId: String,
        @SerializedName("staff_code") val staffCode: String,
        @SerializedName("staff_name") val staffName: String,
        @SerializedName("section_full_name") val sectionFullName: String?,
        @SerializedName("current_flag") val currntFlag: String?
        )