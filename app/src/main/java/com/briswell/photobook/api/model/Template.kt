package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 4/3/2018.
 */
data class Template(
        @SerializedName("id") val id: String,
        @SerializedName("template_name") val template_name: String


)