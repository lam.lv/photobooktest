package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 4/3/2018.
 */
data class TemplateDetail(
        @SerializedName("comment") val comment: String,
        @SerializedName("information") val information: String


)