package com.briswell.photobook.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 4/3/2018.
 */
data class User(
        @SerializedName("user_id") val user_id: String,
        @SerializedName("user_name") val user_name: String,
        @SerializedName("email") val email: String,
        @SerializedName("password") val password: String,
        @SerializedName("user_flag") val user_flag: Int,
        @SerializedName("key") val key: String,
        @SerializedName("api_key") val api_key: String,
        @SerializedName("plan") val plan: String?,
        @SerializedName("company_id") val company_id: String,
        @SerializedName("company_name") val company_name: String?,
        @SerializedName("zip_code") val zip_code: String?,
        @SerializedName("prefecture") val prefecture: String?,
        @SerializedName("city") val city: String?,
        @SerializedName("address1") val address1: String?,
        @SerializedName("address2") val address2: String?,
        @SerializedName("staff_id") val staff_id: Long?,
        @SerializedName("supplier_staff_id") val supplier_staff_id: Long?

)