package com.briswell.photobook.util.api.repository

import android.util.Log
import com.briswell.photobook.BuildConfig
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.LoginResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.RegistUserResult
import com.briswell.photobook.api.model.User
import com.briswell.photobook.util.CryptoUtil
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call
import com.google.gson.JsonObject
import retrofit2.Callback
import retrofit2.Response


/**
 * Repository method to access search functionality of the api service
 */
class AccountRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun login(key: String, email: String, password: String): retrofit2.Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("login")
        val params = HashMap<String, String>()

        params.put("uid", email)
        params.put("encrypted_password", CryptoUtil.md5Enctypt(password))
        return apiService.login(BuildConfig.apiKey,passPhrase,BuildConfig.version,params)
    }



}
