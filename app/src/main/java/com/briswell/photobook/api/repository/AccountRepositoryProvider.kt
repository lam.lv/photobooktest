package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object AccountRepositoryProvider {

    fun getAccountRepository(): AccountRepository {
        return AccountRepository(IJNetApiService.create())
    }

}