package com.briswell.photobook.util.api.repository

import com.briswell.photobook.BuildConfig
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.CryptoUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call


/**
 * Repository method to access search functionality of the api service
 */
class LocationAreaRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getLocationArea(kana:String?,name:String?,maintenance_flag:String?,prefecture:String?,city:String?,address1:String?,page:String,count:String): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_location_area")
        return apiService.getLocationArea(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,kana,name,maintenance_flag,prefecture,city,address1,page.toString(),count.toString())

    }


}