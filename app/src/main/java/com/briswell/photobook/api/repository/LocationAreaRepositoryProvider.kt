package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object LocationAreaRepositoryProvider {

    fun getLocationAreaRepository(): LocationAreaRepository {
        return LocationAreaRepository(IJNetApiService.create())
    }


}