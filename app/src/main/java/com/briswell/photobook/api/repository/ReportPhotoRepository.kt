package com.briswell.photobook.util.api.repository

import android.util.Log
import com.briswell.photobook.BuildConfig
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.LoginResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.RegistUserResult
import com.briswell.photobook.api.model.User
import com.briswell.photobook.util.CryptoUtil
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call
import com.google.gson.JsonObject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Callback
import retrofit2.Response
import java.io.File


/**
 * Repository method to access search functionality of the api service
 */
class ReportPhotoRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getReportPhotoFile(request_id:String,report_photo_file_id:String?,file_path:String?): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_report_photo_file")
        return apiService.getReportPhotoFile(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,request_id,report_photo_file_id,file_path)

    }
    fun getReportPhoto(request_id:String): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_report_photo")
        return apiService.getReportPhoto(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,request_id)

    }
    fun getTemplate(): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_template")
        return apiService.getTemplate(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version)

    }
    fun getTemplateDetail(templateId:String): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_template_detail")
        return apiService.getTemplateDetail(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,templateId)

    }
    fun setReportPhoto(mode:String,id:String?,location_area_id:String?,tts_order_id:String,request_id:String,template_type:String?,customer_name:String?,
                       title:String?,location_name:String?,working_date:String?,invoice_name_suffix:String?,user_id:String?): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("set_report_photo")

        var mode: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), mode)
        var id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), id)
        var location_area_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), location_area_id)
        val tts_order_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), tts_order_id)
        val request_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), request_id)
        val template_type: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), template_type)
        val customer_name: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), customer_name)
        val title: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), title)
        val location_name: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), location_name)
        val working_date: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), working_date)
        val invoice_name_suffix: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), invoice_name_suffix)
        val user_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), user_id)
        return apiService.setReportPhoto(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,mode,id,location_area_id,tts_order_id,request_id,template_type,customer_name,
                title,location_name,working_date,invoice_name_suffix,user_id)

    }
    fun setReportPhotoFile(cookie:String,request_id:String?,file_path:String?,folder_flag:String,user_id:String,folder_id:String?,file:File?): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("set_report_photo_file")

        var request_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), request_id);
        var file_path: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), file_path)
        val folder_flag: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), folder_flag)
        val user_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), user_id)
        val folder_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), folder_id)

        var  image = MultipartBody.Part.createFormData("file", "", RequestBody.create(MediaType.parse("text/plain"), ""))
        if(file != null) {
            image =  MultipartBody.Part.createFormData("file", file!!.getName(), RequestBody.create(MediaType.parse("image/*"), file))
        }

        return apiService.setReportPhotoFile(cookie,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,request_id,file_path,folder_flag,user_id,folder_id,image)

    }
    fun setReportPhotoFile(request_id:String?,file_path:String?,folder_flag:String,user_id:String,folder_id:String?,file:File?): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("set_report_photo_file")

        var request_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), request_id);
        var file_path: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), file_path)
        val folder_flag: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), folder_flag)
        val user_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), user_id)
        val folder_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), folder_id)

        var  image = MultipartBody.Part.createFormData("file", "", RequestBody.create(MediaType.parse("text/plain"), ""))
        if(file != null) {
            image =  MultipartBody.Part.createFormData("file", file!!.getName(), RequestBody.create(MediaType.parse("image/*"), file))
        }

        return apiService.setReportPhotoFile(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,request_id,file_path,folder_flag,user_id,folder_id,image)

    }
    fun updateReportPhotoFile(id:String?,file_path:String?,user_id:String): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("update_report_photo_file")

        var id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), id);
        var file_path: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), file_path)
        val user_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), user_id)

        return apiService.updateReportPhotoFile(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,id,file_path,user_id)

    }
    fun setReportPhotoDetail(mode:String,id:String?,report_photo_id:String?,report_photo_file_id:String,comment:String,manual:String?,sort:String?,user_id:String?): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("set_report_photo_detail")

        var mode: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), mode)
        var id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), id)
        var report_photo_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), report_photo_id)
        val report_photo_file_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), report_photo_file_id)
        val comment: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), comment)
        val manual: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), manual)
        val sort: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), sort)
        val user_id: RequestBody = RequestBody.create(
                MediaType.parse("multipart/form-data"), user_id)
        return apiService.setReportPhotoDetail(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,mode,id,report_photo_id,report_photo_file_id,comment,manual,sort,user_id)

    }
    fun downloadReportPhoto(request_id:String): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("download_report_photo")
        return apiService.downloadReportPhoto(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,request_id)
    }

}
