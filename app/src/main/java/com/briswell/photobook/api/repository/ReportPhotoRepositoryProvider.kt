package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object ReportPhotoRepositoryProvider {

    fun getReportPhotoRepository(): ReportPhotoRepository {
        return ReportPhotoRepository(IJNetApiService.create())
    }


}