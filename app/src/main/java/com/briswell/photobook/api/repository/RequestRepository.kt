package com.briswell.photobook.util.api.repository

import com.briswell.photobook.BuildConfig
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.api.*
import com.briswell.photobook.util.CryptoUtil
import com.briswell.photobook.util.api.model.Request
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.File


/**
 * Repository method to access search functionality of the api service
 */
class RequestRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getRequest(working_date_from:String,working_date_to:String,
                   location_area_name:String,location_area_id:String,
                   tts_order_id:String,section_id:String,staff_id:String,
                   supplier_id:String,page:String): Call<ResponseBody> {
        var passPhrase = CryptoUtil.sha256Encrypt("get_request")
        return apiService.getRequest(
                IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase,BuildConfig.version,
                working_date_from, working_date_to,location_area_name,
                location_area_id,tts_order_id,section_id,staff_id,supplier_id,page)

    }


    fun registContructions(request:Request): io.reactivex.Observable<RegistConstructionResult> {

        var r:RegistConstructionResult? = null;
        return io.reactivex.Observable.just(r!!)
    }
    fun updateContructions(request:Request): io.reactivex.Observable<RegistConstructionResult> {

        var r:RegistConstructionResult? = null;
        return io.reactivex.Observable.just(r!!)
    }


    fun removePhotoDetail(params:HashMap<String, String>): io.reactivex.Observable<BasicResult> {
        return apiService.removePhotoDetail(params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
    fun getReportPhotoDetails(params:HashMap<String, String>): io.reactivex.Observable<ReportPhotoDetailResult> {
        return apiService.photoDetailLists(params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

}