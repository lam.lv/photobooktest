package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object RequestRepositoryProvider {

    fun getRequestRepository(): RequestRepository {
        return RequestRepository(IJNetApiService.create())
    }


}