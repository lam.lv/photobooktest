package com.briswell.photobook.util.api.repository

import com.briswell.photobook.BuildConfig
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.CryptoUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call


/**
 * Repository method to access search functionality of the api service
 */
class SectionRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getSection(hierarchy:String,current_flag:String,page:String): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_section")
        return apiService.getSection(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,hierarchy,current_flag,page)

    }


}