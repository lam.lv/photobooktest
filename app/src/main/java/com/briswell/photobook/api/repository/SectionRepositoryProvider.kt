package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object SectionRepositoryProvider {

    fun getSectionRepository(): SectionRepository {
        return SectionRepository(IJNetApiService.create())
    }


}