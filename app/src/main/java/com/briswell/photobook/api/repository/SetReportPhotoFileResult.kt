package com.briswell.photobook.api.repository

import com.briswell.photobook.api.model.ReportPhotoFile

/**
 * Entire search result data class
 */
data class SetReportPhotoFileResult (
        val status:Long,
        val message:String,
        val count:String,
        val data: ReportPhotoFile?

)