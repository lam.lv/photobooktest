package com.briswell.photobook.util.api.repository

import com.briswell.photobook.BuildConfig
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.CryptoUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call


/**
 * Repository method to access search functionality of the api service
 */
class StaffRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getStaff(staff_name:String?,section_id:String?,current_flag:String?,approve_authority:String?): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_staff")
        return apiService.getStaff(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,staff_name,section_id,current_flag,approve_authority)

    }


}