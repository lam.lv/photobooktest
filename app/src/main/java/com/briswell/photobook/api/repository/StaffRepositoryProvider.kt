package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object StaffRepositoryProvider {

    fun getStaffRepository(): StaffRepository {
        return StaffRepository(IJNetApiService.create())
    }


}