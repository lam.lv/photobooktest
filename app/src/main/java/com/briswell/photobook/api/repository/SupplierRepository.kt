package com.briswell.photobook.util.api.repository

import com.briswell.photobook.BuildConfig
import com.briswell.photobook.api.BasicResult
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.util.CryptoUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call


/**
 * Repository method to access search functionality of the api service
 */
class SupplierRepository(val apiService: IJNetApiService) {

    /**
     *
     */
    fun getSupplier(kana:String,name:String,integration_flag:String?,overtime_work:String?,prefecture:String?,city:String,supplier_tts:String?,page:String,count:String): Call<ResponseBody>{
        var passPhrase = CryptoUtil.sha256Encrypt("get_supplier")
        return apiService.getSupplier(IJNetApiService.REQUEST_COOKIES,
                BuildConfig.apiKey,passPhrase, BuildConfig.version,kana,name,integration_flag!!,overtime_work!!,prefecture!!,city,supplier_tts!!,page.toString(),count.toString())

    }


}