package com.briswell.photobook.util.api.repository

import com.briswell.photobook.api.IJNetApiService


object SupplierRepositoryProvider {

    fun getSupplierRepository(): SupplierRepository {
        return SupplierRepository(IJNetApiService.create())
    }


}