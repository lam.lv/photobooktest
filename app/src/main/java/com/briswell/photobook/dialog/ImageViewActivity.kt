package com.briswell.photobook.dialog

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.briswell.photobook.R
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.activity.BaseActivity
import com.briswell.photobook.activity.S1021Activity
import com.briswell.photobook.activity.S1031Activity
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.model.ReportPhoto
import com.briswell.photobook.api.model.ReportPhotoDetail
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.bumptech.glide.Glide
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*


/**
 * 新規アカウント登録画面
 * 作成者：luc.nt
 * 作成日：20180828
 */

class ImageViewActivity : BaseActivity() {
    private var image: ImageView? = null
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
        image = findViewById(R.id.image)
        var url = this!!.intent.extras.get("url").toString()
        Glide.with(this).load(url).into(image)
    }


}
