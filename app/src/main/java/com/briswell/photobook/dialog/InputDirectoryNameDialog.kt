package com.briswell.photobook.dialog

import android.view.WindowManager
import android.content.DialogInterface
import android.view.LayoutInflater
import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.app.AlertDialog
import android.view.ContextThemeWrapper
import android.widget.EditText
import com.briswell.photobook.R


class InputDirectoryNameDialog(activity: Activity, listener: InputDirectoryNameDialogListener?,update: Boolean) : AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AppTheme)) {

    private val mDirectoryName: EditText

    interface InputDirectoryNameDialogListener {
        fun onOK(name: String)
        fun onCancel()
    }

    init {

        @SuppressLint("InflateParams") // It's OK to use NULL in an AlertDialog it seems...
        val dialogLayout = LayoutInflater.from(activity).inflate(R.layout.dialog_input_directory_name, null)
        setView(dialogLayout)

        mDirectoryName = dialogLayout.findViewById(R.id.directory_name)
        if(!update) {
            setPositiveButton("作成", DialogInterface.OnClickListener { dialog, id ->
                listener?.onOK(mDirectoryName.text.toString())
            })
        }else {
            setPositiveButton("更新", DialogInterface.OnClickListener { dialog, id ->
                listener?.onOK(mDirectoryName.text.toString())
            })
        }


        setNegativeButton("キャンセル", DialogInterface.OnClickListener { dialog, id ->
            listener?.onCancel()
        })
    }

    fun setName(name: String): InputDirectoryNameDialog {
        mDirectoryName.setText(name)
        return this
    }

    override fun show(): AlertDialog {
        val dialog = super.show()
        val window = dialog.getWindow()
        if (window != null)
            window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        return dialog
    }
}