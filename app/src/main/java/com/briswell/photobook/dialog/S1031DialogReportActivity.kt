package com.briswell.photobook.dialog

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.briswell.photobook.R
import com.briswell.photobook.SystemInfo
import com.briswell.photobook.activity.BaseActivity
import com.briswell.photobook.activity.S1021Activity
import com.briswell.photobook.activity.S1031Activity
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.model.ReportPhoto
import com.briswell.photobook.api.model.ReportPhotoDetail
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*


/**
 * 新規アカウント登録画面
 * 作成者：luc.nt
 * 作成日：20180828
 */

class S1031DialogReportActivity : BaseActivity() {

    private lateinit var cancel : Button
    private lateinit var regist : Button
    private lateinit var customerName : EditText
    private lateinit var title:EditText
    private lateinit var locationName:EditText
    private lateinit var workingDate:TextView
    var reportPhoto:ReportPhoto?= null
    var clone:ReportPhoto?= null
    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reportPhoto = Gson().fromJson(this!!.intent.extras.get("report_photo").toString(),ReportPhoto::class.java)
        clone = Gson().fromJson(this!!.intent.extras.get("report_photo").toString(),ReportPhoto::class.java)

        setContentView(R.layout.activity_s1031_dialog_report)
        cancel = findViewById(R.id.cancel)
        regist = findViewById(R.id.regist)
        cancel.setOnClickListener() {
            setData()
            if(isChanged()) {
                this.showMessageOKCancel(resources.getString(R.string.CFRM_004),
                        DialogInterface.OnClickListener { dialog, which ->
                                this.setResult(Activity.RESULT_CANCELED)
                                this.finish()

                        })
            }else {
                this.setResult(Activity.RESULT_CANCELED)
                this.finish()
            }

        }
        regist.setOnClickListener() {
            regist()
            return@setOnClickListener
        }
        customerName = findViewById(R.id.customer_name)
        title = findViewById(R.id.title)
        locationName = findViewById(R.id.location_name)
        workingDate = findViewById(R.id.working_date)

        customerName.setText(reportPhoto!!.customer_name)
        title.setText(reportPhoto!!.title)
        locationName.setText(reportPhoto!!.location_name)
        workingDate.setText(reportPhoto!!.working_date)

        var myCalendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("yyyy/MM/dd")

        val workingDateListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //set value at here
            workingDate.setText(sdf.format(myCalendar.time))
        }
        workingDate.setOnClickListener {
            DatePickerDialog(this, workingDateListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

    }
    private fun isChanged():Boolean {
        if(clone!!.customer_name != reportPhoto!!.customer_name||
                clone!!.location_name != reportPhoto!!.location_name||
                clone!!.title != reportPhoto!!.title||
                clone!!.working_date != reportPhoto!!.working_date) {
            return true
        }
        return false
    }
    fun regist() {
        val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
        var success = true

        var id = ""
        var mode = 1
        if (!reportPhoto!!.id.isNullOrEmpty() && reportPhoto!!.id.toLong() > 0) {
            mode = 2
            id = reportPhoto!!.id.toString()
        }

        setData()

        var call: Call<ResponseBody> = repository.setReportPhoto(mode.toString(), id, reportPhoto!!.location_area_id,
                reportPhoto!!.tts_order_id, reportPhoto!!.request_id, reportPhoto!!.template_type, reportPhoto!!.customer_name, reportPhoto!!.title,
                reportPhoto!!.location_name, reportPhoto!!.working_date, reportPhoto!!.invoice_name_suf, SystemInfo.user.user_id)
        var body: String = call.execute().body()!!.string()
        Log.d("result:", body);
        var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)

        if (result.status != IJNetApiService.ApiStatus.OK.value) {
            success = false
        }else {
            clone = reportPhoto
        }
        var act: BaseActivity = BaseActivity()
        if (success) {
            val resultIntent = Intent(this.getApplicationContext(), S1031Activity::class.java)
            resultIntent.putExtra("report_photo",Gson().toJson(reportPhoto))
            this.setResult(Activity.RESULT_OK, resultIntent)
            act.dialogPopupAndFinishActivity(this,resultIntent, BaseActivity.MESSAGE_TYPE.SUCCESS, resources.getString(R.string.INFO_001))
        } else {
            act.dialogPopup(this, BaseActivity.MESSAGE_TYPE.ERROR, resources.getString(R.string.SERR_001))
        }
    }
    fun setData() {
        reportPhoto!!.customer_name = customerName.text.toString()
        reportPhoto!!.location_name = locationName.text.toString()
        reportPhoto!!.title = title.text.toString()
        reportPhoto!!.working_date = workingDate.text.toString()
    }

}
