package com.briswell.photobook.helper

/**
 * Created by luc.nt on 5/7/2018.
 */

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View
import com.briswell.photobook.activity.S1031Activity
import com.briswell.photobook.adapter.ReportPhotoAdapter
import com.briswell.photobook.adapter.ReportPhotoDetailAdapter
import com.briswell.photobook.model.IDragDropModel
import java.util.*

class DragListener internal constructor(private val activity:Activity,private val selfView: RecyclerView): View.OnDragListener {

    private var isDropped = false
    private var isMove = false

    override fun onDrag(v: View, event: DragEvent): Boolean {
        when (event.action) {
            DragEvent.ACTION_DROP -> {
                isDropped = true
                var target: RecyclerView = selfView
                val viewSource = event.localState as View
                if (viewSource != null) {
                    val source = viewSource.parent as RecyclerView
                    if(viewSource.tag != null) {
                        if (source!!.adapter is ReportPhotoAdapter) {

                            var adapterSource = source!!.adapter as ReportPhotoAdapter
                            val positionSource = viewSource.tag as Int
                            var it = adapterSource.dataset!!.get(positionSource)!!
                            val newItem = IDragDropModel(id = 0,file_path = it.file_path,
                                    file_name = "",
                                    thumbnail_file_path = it.thumbnail_file_path,comment = it.comment,
                                    manual = it.manual,upload_status = it.upload_status,
                                    checked = false,report_photo_file_id = it.report_photo_file_id,
                                    childrens = null,parent = null,isDirectory = false,display = "",useNum = 0,checkNum = 0)
                            val listSource = adapterSource.dataset!! as MutableList<IDragDropModel?>?
                            if (isMove) {
                                listSource!!.removeAt(positionSource)
                                adapterSource.updateDataSet(listSource)
                            }
                            if (target!!.adapter is ReportPhotoDetailAdapter) {
                                val adapterTarget = target!!.adapter as ReportPhotoDetailAdapter

                                val customListTarget = adapterTarget.dataset!!
                                var dropedView = selfView.findChildViewUnder(event.x,event.y)
                                var positionTarget = -1
                                if(dropedView != null) {
                                    positionTarget = selfView.getChildAdapterPosition(dropedView)
                                }
                                if (positionTarget >= 0) {
                                    if(customListTarget[positionTarget]!!.file_path.isNullOrEmpty()) {
                                        customListTarget[positionTarget]!!.report_photo_file_id = newItem.report_photo_file_id
                                        customListTarget[positionTarget]!!.file_path = newItem.file_path
                                        customListTarget[positionTarget]!!.thumbnail_file_path = newItem.thumbnail_file_path
                                        adapterTarget.dataset = customListTarget
                                        adapterTarget.notifyItemRangeChanged(0,adapterTarget.dataset!!.size)
                                    }else {

                                        customListTarget.add(positionTarget, newItem)
                                        adapterTarget.dataset = customListTarget
                                        adapterTarget.notifyItemInserted(positionTarget)
                                        if(positionTarget <adapterTarget.dataset!!.size)
                                        {
                                            adapterTarget.notifyItemRangeChanged(positionTarget+1,adapterTarget.dataset!!.size -(positionTarget+1))
                                        }
                                    }
                                } else {
                                    customListTarget.add(newItem)
                                    adapterTarget.dataset = customListTarget
                                    adapterTarget.notifyItemInserted(adapterTarget.itemCount)
                                }

                            }

                        } else if (source!!.adapter is ReportPhotoDetailAdapter) {
                            val positionSource = selfView.getChildAdapterPosition(viewSource)
                            var targetView = selfView.findChildViewUnder(event.x,event.y)
                            var positionTarget = -1
                            if(targetView != null) {
                                positionTarget = selfView.getChildAdapterPosition(targetView)
                            }
                            Log.d("drag index",positionSource.toString())
                            Log.d("droped index",positionTarget.toString())
                            if (target!!.adapter is ReportPhotoDetailAdapter) {
                                var adapterTarget = target!!.adapter as ReportPhotoDetailAdapter
                                if(positionTarget>=0 && positionSource != positionTarget) {

                                    val customListTarget = adapterTarget.dataset!!
                                    var tmpSrc = customListTarget[positionSource]!!.copy()
                                    var tmpTarg = customListTarget[positionTarget]!!.copy()
                                    customListTarget[positionTarget]!!.id = tmpSrc.id
                                    customListTarget[positionTarget]!!.report_photo_file_id = tmpSrc.report_photo_file_id
                                    customListTarget[positionTarget]!!.file_path = tmpSrc.file_path
                                    customListTarget[positionTarget]!!.comment = tmpSrc.comment
                                    customListTarget[positionTarget]!!.manual = tmpSrc.manual


                                    customListTarget[positionSource]!!.id = tmpTarg.id
                                    customListTarget[positionSource]!!.report_photo_file_id = tmpTarg.report_photo_file_id
                                    customListTarget[positionSource]!!.file_path = tmpTarg.file_path
                                    customListTarget[positionSource]!!.comment = tmpTarg.comment
                                    customListTarget[positionSource]!!.manual = tmpTarg.manual
                                    adapterTarget.dataset = customListTarget
                                    adapterTarget.notifyItemChanged(positionTarget,true)
                                    adapterTarget.notifyItemChanged(positionSource,true)



                                }
                            }

                        }
                        if(activity is S1031Activity) {
                            (activity as S1031Activity).resetUseNum()
                        }
                    }
                }
            }
            DragEvent.ACTION_DRAG_ENTERED-> {
                Log.d("Entered",v?.toString())
            }

        }
        if (!isDropped && event.localState != null) {
            (event.localState as View).visibility = View.VISIBLE
        }
        return true
    }



}

