package com.briswell.photobook.helper

/**
 * Created by luc.nt on 5/24/2018.
 */
interface OnLoadMoreListener {
    fun onLoadMore()
}