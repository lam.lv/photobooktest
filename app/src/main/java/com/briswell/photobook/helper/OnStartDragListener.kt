package com.briswell.photobook.helper

/**
 * Created by luc.nt on 5/9/2018.
 */

import android.support.v7.widget.RecyclerView

/**
 * Listener for manual initiation of a drag.
 */
interface OnStartDragListener {

    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder)

}