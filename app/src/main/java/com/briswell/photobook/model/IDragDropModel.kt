package com.briswell.photobook.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 5/8/2018.
 */
data class IDragDropModel (
        @SerializedName("checked") var checked:Boolean?,
        @SerializedName("id") var id:Long?,
        @SerializedName("file_path") var file_path:String?,
        @SerializedName("file_name") var file_name:String?,
        @SerializedName("thumbnail_file_path") var thumbnail_file_path:String?,
        @SerializedName("upload_status") var upload_status:Int,
        @SerializedName("comment") var comment:String?,
        @SerializedName("manual") var manual:String?,
        @SerializedName("report_photo_file_id") var report_photo_file_id:Long?,
        var childrens: MutableList<IDragDropModel?>?,
        var parent: IDragDropModel?,
        var isDirectory: Boolean?,
        @SerializedName("display") var display:String?,
        @SerializedName("useNum") var useNum:Int,
        @SerializedName("checkNum") var checkNum:Int,
        @SerializedName("isLabel") var isLabel:Boolean = false

)