package com.briswell.photobook.model

import com.google.gson.annotations.SerializedName

/**
 * Created by luc.nt on 10/31/2018.
 */
data class SendBroadcastReceiverModel (
        @SerializedName("cookie") var cookie:String?,
        @SerializedName("request_id") var request_id:String?,
        @SerializedName("path") var path:String?,
        @SerializedName("folder_flg") var folder_flg:String?,
        @SerializedName("user_id") var user_id:String?,
        @SerializedName("folder_id") var folder_id:String?,
        @SerializedName("local_file_path") var local_file_path:String?
)