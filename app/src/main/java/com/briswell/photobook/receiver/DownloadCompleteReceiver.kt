package com.briswell.photobook.receiver

import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.app.DownloadManager


/**
 * Created by luc.nt on 10/31/2018.
 */
class DownloadCompleteReceiver : BroadcastReceiver() {
    var downloadId:Long?=null
    lateinit var alertDialogPopup: android.support.v7.app.AlertDialog
    override fun onReceive(context: Context, intent: Intent) {
        val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
        //Checking if the received broadcast is for our enqueued download by matching download id
        if (downloadId == id) {
            Toast.makeText(context, "ダウンロードが完了しました。", Toast.LENGTH_SHORT).show()
        }
    }

}