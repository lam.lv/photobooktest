package com.briswell.photobook.receiver

import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.briswell.photobook.api.IJNetApiService
import com.briswell.photobook.api.repository.SetReportPhotoFileResult
import com.briswell.photobook.model.SendBroadcastReceiverModel
import com.briswell.photobook.util.Constant
import com.briswell.photobook.util.PreferencesHelper
import com.briswell.photobook.util.api.repository.ReportPhotoRepositoryProvider
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.File


/**
 * Created by luc.nt on 10/31/2018.
 */
class NetworkChangeReceiver : BroadcastReceiver() {
    lateinit var alertDialogPopup: android.support.v7.app.AlertDialog
    override fun onReceive(context: Context, intent: Intent) {

        val connMgr = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connMgr.getActiveNetworkInfo()
        if(netInfo != null && netInfo.isConnected()) {
            Toast.makeText(context, "インターネットに接続されました。", Toast.LENGTH_SHORT).show()
            var json = PreferencesHelper.getString(context,Constant.UPLOAD_PENDING_PHOTO_DATA)
            if(!json.isNullOrEmpty()) {
                val repository = ReportPhotoRepositoryProvider.getReportPhotoRepository()
                var items:MutableList<SendBroadcastReceiverModel> = mutableListOf()
                items = Gson().fromJson(json, object : TypeToken<List<SendBroadcastReceiverModel>>() {}.type)
                var newfile:File? = null
                var picturePath:String? = null
                var flg = true
                for (i in 0 until items.count()) {
                    picturePath = items[i].local_file_path
                    newfile = File(picturePath)
                    var call: Call<ResponseBody> = repository.setReportPhotoFile(items[i].cookie!!,items[i].request_id, items[i].path, items[i].folder_flg.toString(), items[i].user_id.toString(), items[i].folder_id, newfile!!)
                    var body: String = call.execute().body()!!.string()
                    Log.d("result:", body)
                    var result: SetReportPhotoFileResult = Gson().fromJson(body, SetReportPhotoFileResult::class.java)
                    if(result.status != IJNetApiService.ApiStatus.OK.value) {
                        flg = false
                    }

                }
                if(flg) {
                    PreferencesHelper.putString(context,Constant.UPLOAD_PENDING_PHOTO_DATA,"")
                    Toast.makeText(context, "アップロード完了しました。", Toast.LENGTH_SHORT).show()

                }else {
                    Toast.makeText(context, "アップロードできません。", Toast.LENGTH_SHORT).show()
                }
            }
        }else {
            Toast.makeText(context, "インターネットに接続されません。", Toast.LENGTH_SHORT).show()
        }



    }

}