package com.briswell.photobook.util

import android.content.Context

/**
 * Created by luc.nt on 5/24/2018.
 */
object Constant {
    val VIEW_TYPE_ITEM = 0
    val VIEW_TYPE_LOADING = 1
    val VIEW_TYPE_DISABLE = -1
    val APP_PREF_NAME = "IJNET_DATA"
    var APP_CONTEXT:Context? = null
    val UPLOAD_PENDING_PHOTO_DATA = "BROADCAST_RECEIVER_PHOTO_DATA"
    val BROADCAST_RECEIVER_IMAGE_SEND_ACTION = "ijnet.photobook.image.send"
}