package com.briswell.photobook.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by luc.nt on 4/10/2018.
 */
class ConvertUtils {
    companion object DateUtils {
        fun getDate(source:String?,sourceFormat:String):Date {
            var format = SimpleDateFormat(sourceFormat)
            return  format.parse(source) as Date
        }
        fun getDateString(source:Date,destinationFormat:String):String {
            var format = SimpleDateFormat(destinationFormat)
            return format.format(source)
        }
    }

}