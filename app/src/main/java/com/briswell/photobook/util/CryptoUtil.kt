package com.briswell.photobook.util
import java.math.BigInteger
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException


/**
 * Created by luc.nt on 9/10/2018.
 */
class CryptoUtil {
    companion object CryptoUtil {
        fun sha256Encrypt(plantext:String): String {
            var md: MessageDigest? = null
            var sb: StringBuilder? = null
            try {
                md = MessageDigest.getInstance("SHA-256")
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            md!!.update(plantext.toByteArray())
            sb = StringBuilder()
            for (b in md!!.digest()) {
                val hex = String.format("%02x", b)
                sb.append(hex)
            }
            return sb.toString()

        }

        fun md5Enctypt(s: String): String {
            var m: MessageDigest? = null

            try {
                m = MessageDigest.getInstance("MD5")
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            m!!.update(s.toByteArray(), 0, s.length)
            return BigInteger(1, m.digest()).toString(16).padStart(32, '0')
        }

    }

}