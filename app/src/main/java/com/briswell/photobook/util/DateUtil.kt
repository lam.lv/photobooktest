package com.briswell.photobook.util

import java.util.*

/**
 * Created by luc.nt on 9/13/2018.
 */
class DateUtil {
    companion object DateUtil {

        fun getDayOfWeek(date:Date): String {
            var cal = Calendar.getInstance()
            cal.time = date
            when (cal.get(Calendar.DAY_OF_WEEK)) {
                Calendar.SUNDAY -> return "日曜日"
                Calendar.MONDAY -> return "月曜日"
                Calendar.TUESDAY -> return "火曜日"
                Calendar.WEDNESDAY -> return "水曜日"
                Calendar.THURSDAY -> return "木曜日"
                Calendar.FRIDAY -> return "金曜日"
                Calendar.SATURDAY -> return "土曜日"
            }
            throw IllegalStateException()
        }

        /**
         * 現在の曜日を返します。
         * ※曜日は省略します。
         * @return    現在の曜日
         */
        fun getShortDayOfWeek(date:Date): String {
            var cal = Calendar.getInstance()
            cal.time = date
            when (cal.get(Calendar.DAY_OF_WEEK)) {
                Calendar.SUNDAY -> return "日"
                Calendar.MONDAY -> return "月"
                Calendar.TUESDAY -> return "火"
                Calendar.WEDNESDAY -> return "水"
                Calendar.THURSDAY -> return "木"
                Calendar.FRIDAY -> return "金"
                Calendar.SATURDAY -> return "土"
            }
            throw IllegalStateException()
        }
    }
}