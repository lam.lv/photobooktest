package com.briswell.photobook.util
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import com.briswell.photobook.SystemInfo
import java.util.*
import java.util.regex.Pattern
import android.widget.Toast
import android.graphics.Bitmap
import android.nfc.Tag
import android.util.Log
import okhttp3.ResponseBody
import java.io.*


/**
 * Created by luc.nt on 4/18/2018.
 */
class FileUtils {
    companion object FileUtils {
        fun download(fileUrl:String): File? {
            val fileName = URLUtil.guessFileName(fileUrl, null, null)
            // check folder exist
            val dir = File(Environment.getExternalStorageDirectory().absolutePath + "/" + SystemInfo.Constant.APP_FOLDER_NAME)
            if (!(dir.exists() && dir.isDirectory)) {
                dir.mkdir()
            }
            val file = File(dir.path+ "/" + fileName)

            try {
                val outStream = FileOutputStream(file)
                outStream.flush()
                outStream.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file

        }

        fun writeResponseBodyToDisk(body: ResponseBody,fileName:String): Boolean {
            try {
                // todo change the file location/name according to your needs
                val dir  = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS!!).toString())
                if (!(dir.exists() && dir.isDirectory)) {
                    dir.mkdir()
                }
                val file = File(dir.path+ File.separator + fileName)
                if (!file.exists()) file.createNewFile()

                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null

                try {
                    val fileReader = ByteArray(4096)

                    val fileSize = body.contentLength()
                    var fileSizeDownloaded: Long = 0

                    inputStream = body.byteStream()
                    outputStream = FileOutputStream(file)

                    while (true) {
                        val read = inputStream!!.read(fileReader)

                        if (read == -1) {
                            break
                        }

                        outputStream!!.write(fileReader, 0, read)

                        fileSizeDownloaded += read.toLong()

                        Log.d("download info:", "file download: $fileSizeDownloaded of $fileSize")
                    }

                    outputStream!!.flush()

                    return true
                } catch (e: IOException) {
                    return false
                } finally {
                    if (inputStream != null) {
                        inputStream!!.close()
                    }

                    if (outputStream != null) {
                        outputStream.close()
                    }
                }
            } catch (e: IOException) {
                return false
            }

        }

    }

}