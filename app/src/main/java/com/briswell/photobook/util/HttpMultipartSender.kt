package info.informationsea.apps

import java.io.DataOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.ProtocolException
import java.util.UUID

/**
 * Copyright (C) 2014 Yasunobu OKAMURA
 * MIT License
 */
object HttpMultipartSender {

    // this function is implemented based on http://www.androidsnippets.com/multipart-http-requests
    @Throws(IOException::class)
    fun sendMultipart(connection: HttpURLConnection, fileName: String, file: File, textdata: Map<String, String>) {

        val twoHyphens = "--"
        val boundary = "*****" + UUID.randomUUID().toString() + "*****"
        val lineEnd = "\r\n"
        val maxBufferSize = 1024 * 1024 * 3

        val outputStream: DataOutputStream

        connection.doInput = true
        connection.doOutput = true
        connection.useCaches = false

        connection.requestMethod = "POST"
        connection.setRequestProperty("Connection", "Keep-Alive")
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=$boundary")

        outputStream = DataOutputStream(connection.outputStream)
        outputStream.writeBytes(twoHyphens + boundary + lineEnd)
        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + fileName + "\"; filename=\"" + file.name + "\"" + lineEnd)
        outputStream.writeBytes("Content-Type: application/octet-stream$lineEnd")
        outputStream.writeBytes("Content-Transfer-Encoding: binary$lineEnd")
        outputStream.writeBytes(lineEnd)

        val fileInputStream = FileInputStream(file)
        var bytesAvailable = fileInputStream.available()
        var bufferSize = Math.min(bytesAvailable, maxBufferSize)
        val buffer = ByteArray(bufferSize)

        var bytesRead = fileInputStream.read(buffer, 0, bufferSize)
        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize)
            bytesAvailable = fileInputStream.available()
            bufferSize = Math.min(bytesAvailable, maxBufferSize)
            bytesRead = fileInputStream.read(buffer, 0, bufferSize)
        }

        outputStream.writeBytes(lineEnd)

        for ((key, value) in textdata) {
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)
            outputStream.writeBytes("Content-Disposition: form-data; name=\"$key\"$lineEnd")
            outputStream.writeBytes("Content-Type: text/plain$lineEnd")
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(value)
            outputStream.writeBytes(lineEnd)
        }

        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)

        outputStream.close()
    }
}// cannot make instance