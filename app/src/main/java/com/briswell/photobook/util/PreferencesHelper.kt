package com.briswell.photobook.util

import android.content.Context
import android.content.SharedPreferences


/**
 * Created by luc.nt on 9/12/2018.
 */
class PreferencesHelper {


    companion object PreferencesHelper {
        private var mPref: SharedPreferences? = null
        fun putString(context:Context,key:String, value: String) {
            mPref = context.getSharedPreferences(Constant.APP_PREF_NAME, Context.MODE_PRIVATE);
            mPref!!.edit().putString(key, value).apply()
        }
        fun putStringSet(context:Context,key:String, value: Set<String>) {
            mPref = context.getSharedPreferences(Constant.APP_PREF_NAME, Context.MODE_PRIVATE);
            mPref!!.edit().putStringSet(key, value).apply()
        }
        fun getString(context:Context,key:String):String? {
            mPref = context.getSharedPreferences(Constant.APP_PREF_NAME, Context.MODE_PRIVATE)
            if(mPref != null && mPref!!.contains(key)) {
                return mPref!!.getString(key, null)
            }else {
                return null
            }
            return null
        }
        fun getStringSet(context:Context,key:String):Set<String> {
            mPref = context.getSharedPreferences(Constant.APP_PREF_NAME, Context.MODE_PRIVATE)
            if(mPref != null && mPref!!.contains(key)) {
                return mPref!!.getStringSet(key, null)
            }else {
                return setOf()
            }
            return setOf()

        }
    }
}