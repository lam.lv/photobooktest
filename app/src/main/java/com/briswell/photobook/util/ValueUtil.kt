package com.briswell.photobook.util

import com.briswell.photobook.api.model.SpinnerItem


/**
 * Created by luc.nt on 9/10/2018.
 */
class ValueUtil {
    companion object ValueUtil {
        fun getPrefectureItems(): List<SpinnerItem>{
            var PrefectureItems:ArrayList<SpinnerItem> = arrayListOf()
            PrefectureItems.add(SpinnerItem("0", "---"))
            PrefectureItems.add(SpinnerItem("1", "北海道"))
            PrefectureItems.add(SpinnerItem("2", "青森県"))
            PrefectureItems.add(SpinnerItem("3", "岩手県"))
            PrefectureItems.add(SpinnerItem("4", "宮城県"))
            PrefectureItems.add(SpinnerItem("5", "秋田県"))
            PrefectureItems.add(SpinnerItem("6", "山形県"))
            PrefectureItems.add(SpinnerItem("7", "福島県"))
            PrefectureItems.add(SpinnerItem("8", "茨城県"))
            PrefectureItems.add(SpinnerItem("9", "栃木県"))
            PrefectureItems.add(SpinnerItem("10", "群馬県"))
            PrefectureItems.add(SpinnerItem("11", "埼玉県"))
            PrefectureItems.add(SpinnerItem("12", "千葉県"))
            PrefectureItems.add(SpinnerItem("13", "東京都"))
            PrefectureItems.add(SpinnerItem("14", "神奈川県"))
            PrefectureItems.add(SpinnerItem("15", "山梨県"))
            PrefectureItems.add(SpinnerItem("16", "長野県"))
            PrefectureItems.add(SpinnerItem("17", "新潟県"))
            PrefectureItems.add(SpinnerItem("18", "富山県"))
            PrefectureItems.add(SpinnerItem("19", "石川県"))
            PrefectureItems.add(SpinnerItem("20", "福井県"))
            PrefectureItems.add(SpinnerItem("21", "岐阜県"))
            PrefectureItems.add(SpinnerItem("22", "静岡県"))
            PrefectureItems.add(SpinnerItem("23", "愛知県"))
            PrefectureItems.add(SpinnerItem("24", "三重県"))
            PrefectureItems.add(SpinnerItem("25", "滋賀県"))
            PrefectureItems.add(SpinnerItem("26", "京都府"))
            PrefectureItems.add(SpinnerItem("27", "大阪府"))
            PrefectureItems.add(SpinnerItem("28", "兵庫県"))
            PrefectureItems.add(SpinnerItem("29", "奈良県"))
            PrefectureItems.add(SpinnerItem("30", "和歌山県"))
            PrefectureItems.add(SpinnerItem("31", "鳥取県"))
            PrefectureItems.add(SpinnerItem("32", "島根県"))
            PrefectureItems.add(SpinnerItem("33", "岡山県"))
            PrefectureItems.add(SpinnerItem("34", "広島県"))
            PrefectureItems.add(SpinnerItem("35", "山口県"))
            PrefectureItems.add(SpinnerItem("36", "徳島県"))
            PrefectureItems.add(SpinnerItem("37", "香川県"))
            PrefectureItems.add(SpinnerItem("38", "愛媛県"))
            PrefectureItems.add(SpinnerItem("39", "高知県"))
            PrefectureItems.add(SpinnerItem("40", "福岡県"))
            PrefectureItems.add(SpinnerItem("41", "佐賀県"))
            PrefectureItems.add(SpinnerItem("42", "長崎県"))
            PrefectureItems.add(SpinnerItem("43", "熊本県"))
            PrefectureItems.add(SpinnerItem("44", "大分県"))
            PrefectureItems.add(SpinnerItem("45", "宮崎県"))
            PrefectureItems.add(SpinnerItem("46", "鹿児島県"))
            PrefectureItems.add(SpinnerItem("47", "沖縄県"))
            PrefectureItems.add(SpinnerItem("99", "その他"))
            return PrefectureItems

        }

        fun getSuppliersTts(): List<SpinnerItem>{
            var suppliers:ArrayList<SpinnerItem> = arrayListOf()

            suppliers.add(SpinnerItem("0", "--"));
            suppliers.add(SpinnerItem("1", "協力業者・仕入先"));
            suppliers.add(SpinnerItem("2", "TTS営業"));
            suppliers.add(SpinnerItem("3", "TTS技術"));
            return suppliers

        }
        fun getOverTimeWorks(): List<SpinnerItem>{
            var overTimeWorks:ArrayList<SpinnerItem> = arrayListOf()

            overTimeWorks.add(SpinnerItem("", "--"));
            overTimeWorks.add(SpinnerItem("1", "夜間対応可"));
            overTimeWorks.add(SpinnerItem("2", "休日対応可"));
            overTimeWorks.add(SpinnerItem("1,2", "夜間休日対応可"));
            return overTimeWorks

        }
        fun getPageRecords(): List<SpinnerItem>{
            var records:ArrayList<SpinnerItem> = arrayListOf()

            records.add(SpinnerItem("10", "10件"));
            records.add(SpinnerItem("20", "20件"));
            records.add(SpinnerItem("50", "50件"));
            records.add(SpinnerItem("100", "100件"));
            return records

        }

        fun getApproveAuthority(): List<SpinnerItem>{
            var items :ArrayList<SpinnerItem> = arrayListOf()

            items.add(SpinnerItem("", "--"));
            items.add(SpinnerItem("1", "担当者"))
            items.add(SpinnerItem("2", "部門長"))
            items.add(SpinnerItem("3", "所属長"))
            items.add(SpinnerItem("4", "部門長かつ所属長"))
            return items

        }
        fun getMaintenanceFlags(): List<SpinnerItem>{
            var items :ArrayList<SpinnerItem> = arrayListOf()

            items.add(SpinnerItem("", "--"));
            items.add(SpinnerItem("1", "保守"))
            items.add(SpinnerItem("2", "保守外"))
            return items

        }

        fun  getPrefectures():HashMap<String,String> {
            var prefectures:HashMap<String,String> = hashMapOf()
            prefectures.put("0", "---")
            prefectures.put("1", "北海道")
            prefectures.put("2", "青森県")
            prefectures.put("3", "岩手県")
            prefectures.put("4", "宮城県")
            prefectures.put("5", "秋田県")
            prefectures.put("6", "山形県")
            prefectures.put("7", "福島県")
            prefectures.put("8", "茨城県")
            prefectures.put("9", "栃木県")
            prefectures.put("10", "群馬県")
            prefectures.put("11", "埼玉県")
            prefectures.put("12", "千葉県")
            prefectures.put("13", "東京都")
            prefectures.put("14", "神奈川県")
            prefectures.put("15", "山梨県")
            prefectures.put("16", "長野県")
            prefectures.put("17", "新潟県")
            prefectures.put("18", "富山県")
            prefectures.put("19", "石川県")
            prefectures.put("20", "福井県")
            prefectures.put("21", "岐阜県")
            prefectures.put("22", "静岡県")
            prefectures.put("23", "愛知県")
            prefectures.put("24", "三重県")
            prefectures.put("25", "滋賀県")
            prefectures.put("26", "京都府")
            prefectures.put("27", "大阪府")
            prefectures.put("28", "兵庫県")
            prefectures.put("29", "奈良県")
            prefectures.put("30", "和歌山県")
            prefectures.put("31", "鳥取県")
            prefectures.put("32", "島根県")
            prefectures.put("33", "岡山県")
            prefectures.put("34", "広島県")
            prefectures.put("35", "山口県")
            prefectures.put("36", "徳島県")
            prefectures.put("37", "香川県")
            prefectures.put("38", "愛媛県")
            prefectures.put("39", "高知県")
            prefectures.put("40", "福岡県")
            prefectures.put("41", "佐賀県")
            prefectures.put("42", "長崎県")
            prefectures.put("43", "熊本県")
            prefectures.put("44", "大分県")
            prefectures.put("45", "宮崎県")
            prefectures.put("46", "鹿児島県")
            prefectures.put("47", "沖縄県")
            prefectures.put("99", "その他")
            return prefectures
        }
    }

}